//
//  LvlParser.swift
//  Color Killer
//
//  Created by Rafael Seara on 16/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class LvlParser{
    
    struct LevelInfo : Codable {
        struct Node : Codable {
            var nodeType : String
            var xPosition : Int
            var colorXPos  : Int
            var hitsToDie : Int
        }
        
        var pickerName: String
        var speedCall: Int
        var speedFall: Int
        var colorTresh: Double
        var spriteDivisions : Int
        var rows: [[Node]]
    }
    
    private var levelInfo : LevelInfo!

    public func getPickerName() -> String {
        return levelInfo.pickerName
    }
    
    public func getSpeedCall() -> Int {
        return levelInfo.speedCall
    }
    
    public func getSpeedFall() -> Int {
        return levelInfo.speedFall
    }
    
    public func getColorTresh() -> Double {
        return levelInfo.colorTresh
    }
    
    public func getSpriteDivisions() -> Int {
        return levelInfo.spriteDivisions
    }
    
    public func getLevelRowns() -> [[LevelInfo.Node]] {
        return levelInfo.rows
    }
    
    public func parseLvl(lvlName: String, scene: SKScene) -> [[Any]]{
        let file = Bundle.main.url(forResource: lvlName, withExtension: "json")
        var nodesByRow = [[Any]]()
        do {
            let data = try Data(contentsOf: file!)
            let decoder = JSONDecoder()
            levelInfo = try decoder.decode(LevelInfo.self, from: data)
            
            for row in levelInfo.rows{
                var rowNodes = [Any]()
                for element in row{
                    
                    let gameMetric = scene.frame.width / CGFloat(levelInfo.spriteDivisions)
                    
                    let nodeSize = CGSize(width: gameMetric, height: gameMetric)
                    
                    if(element.xPosition > levelInfo.spriteDivisions){
                        fatalError("xPosition higher than divisions")
                    }
                    
                    let xPos = CGFloat(element.xPosition) * gameMetric
                    let position = CGPoint(x: xPos, y: scene.frame.maxY)
                    
                    let colorXPos = CGFloat(element.colorXPos) * gameMetric
                    
                    switch(element.nodeType){
                    case NodeNames.NormalEnemy:
                        let node = NormalLittleEnemy(colorXPos: colorXPos, position: position, size: nodeSize)
                        rowNodes.append(node)
                    case NodeNames.MultEnemy:
                        let node = MultipleHitsEnemy(colorXPos: colorXPos, position: position, size: nodeSize, hitsToDie: element.hitsToDie)
                        rowNodes.append(node)
                    case NodeNames.MeteorEnemy:
                        let node = MeteorEnemy(colorXPos: colorXPos, position: position, size: nodeSize)
                        rowNodes.append(node)
                    case NodeNames.IceCube:
                        let node = IceCube(colorXPos: colorXPos, position: position, size: nodeSize)
                        rowNodes.append(node)
                    case NodeNames.Bomb:
                        let node = Bomb(colorXPos: colorXPos, position: position, size: nodeSize)
                        rowNodes.append(node)
                    case NodeNames.ColorAll:
                        let node = ColorAll(colorXPos: colorXPos, position: position, size: nodeSize)
                        rowNodes.append(node)
                    case NodeNames.Shield:
                        let node = Shield(colorXPos: colorXPos, position: position, size: nodeSize)
                        rowNodes.append(node)
                    case NodeNames.Life:
                        let node = Life(colorXPos: colorXPos, position: position, size: nodeSize)
                        rowNodes.append(node)
                    default:
                        fatalError("Bad import of node")
                    }
                }
                
                nodesByRow.append(rowNodes)
            }
            
            return nodesByRow
        } catch {
            //handle error
            print(error)
        }
        return nodesByRow
    }
    
}
