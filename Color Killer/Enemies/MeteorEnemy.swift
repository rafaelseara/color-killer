//
//  MeteorEnemy.swift
//  Color Killer
//
//  Created by Rafael Seara on 11/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class MeteorEnemy: LittleEnemyNode {
    var fire: SKSpriteNode!
    
    init(color: UIColor, position: CGPoint, size: CGSize) {
        super.init(image: nil, color: color, position: position, size: size)
        self.setupNode()
    }
    
    var colorXPos: CGFloat = 0

    init(colorXPos: CGFloat, position: CGPoint, size: CGSize) {
        super.init(image: nil, color: .red, position: position, size: size)
        self.colorXPos = colorXPos
        self.setupNode()
    }
    
    private func setupNode() {
        physicsBody?.categoryBitMask = PhysicsCategories.meteorEnemy
        name = NodeNames.MeteorEnemy
        //add fire
        //        if let fire = SKEmitterNode(fileNamed: "FireEmitter") {
        //            fire.position = CGPoint(x: 0, y: self.frame.height)
        //            addChild(fire)
        //        }
        self.fire = SKSpriteNode(texture: SKTexture(imageNamed: "Fire"), size: CGSize(width: self.circle!.frame.width, height: self.circle!.frame.height * 2))
        self.fire.position = CGPoint(x: 0, y: self.fire.frame.height / 2 + self.circle!.frame.height / 2 - 5)
        self.circle!.addChild(self.fire)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func go() {
        let moveEnemyAction = SKAction.moveTo(y: (scene?.frame.minY)! - 20, duration: TimeInterval((scene?.frame)!.maxY/100)/2.5)
        self.run(
            SKAction.sequence([
                moveEnemyAction,
                SKAction.removeFromParent()
                ])
        )
    }
    
    func removeFire() {
        self.removeAllActions()
        self.isPaused = false
        self.fire.run(SKAction.resize(toWidth: 0, height: 0, duration: 1), completion: {
            self.fire.removeFromParent()
        })
    }
}

