//
//  MultipleHitsEnemy.swift
//  Color Killer
//
//  Created by Rafael Seara on 10/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class MultipleHitsEnemy: LittleEnemyNode {
    
    var hitsToDie: Int
    var hitLabel: SKLabelNode
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(color: UIColor, position: CGPoint, size: CGSize, hitsToDie: Int) {
        self.hitsToDie = hitsToDie
        self.hitLabel = SKLabelNode()
        super.init(image: nil, color: color, position: position, size: size)
        self.setupNode()
    }
    
    var colorXPos: CGFloat = 0

    init(colorXPos: CGFloat, position: CGPoint, size: CGSize, hitsToDie: Int) {
        self.colorXPos = colorXPos
        self.hitsToDie = hitsToDie
        self.hitLabel = SKLabelNode()
        super.init(image: nil, color: .red, position: position, size: size)
        self.setupNode()
    }
    
    private func setupNode() {
        addHitsLabel()
        physicsBody?.categoryBitMask = PhysicsCategories.multEnemy
        name = NodeNames.MultEnemy
    }
    
    private func addHitsLabel(){
        hitLabel = SKLabelNode(fontNamed: "Thonburi")
        self.addChild(hitLabel)
        hitLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        hitLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.center
        hitLabel.text = String(hitsToDie)
        hitLabel.fontSize = 20
        hitLabel.fontColor = SKColor.black
    }
    
    func hit(){
        if (hitsToDie > 1){
            hitsToDie = hitsToDie - 1
            hitLabel.text = String(hitsToDie)
        }else{
            self.removeFromParent()
        }
    }
    
    func getHitsToDie() -> Int {
        return hitsToDie
    }
}

