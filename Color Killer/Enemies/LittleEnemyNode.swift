//
//  LittleEnemy.swift
//  Color Killer
//
//  Created by Rafael Seara on 09/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class LittleEnemyNode : SKSpriteNode {
    
    enum Error: Swift.Error {
        case imageOrColor
    }
    
    let circle: SKShapeNode?

    init(image: String? = nil, color: UIColor? = nil, position: CGPoint, size: CGSize){
        
        circle = SKShapeNode(circleOfRadius: size.height / 2)
        
        if (image != nil && color == nil) {
            super.init(texture: SKTexture(imageNamed: image!), color: .clear, size: size)
            
        }else if (color != nil  && image == nil) {
            
            super.init(texture: nil, color: color!, size: .zero)
            
            circle!.strokeColor = .clear
            circle!.glowWidth = 1.0
            circle!.fillColor = self.color
            circle!.zPosition = -1
            self.addChild(circle!)
            
            
        }else {
            fatalError("error initializing little enemy")
        }
        
        self.position = position
        
        physicsBody = SKPhysicsBody(circleOfRadius: size.height / 2)
        physicsBody?.isDynamic = true
        physicsBody?.friction = 0
        physicsBody?.linearDamping = 0
        physicsBody?.affectedByGravity = false
        physicsBody?.allowsRotation = false
        physicsBody?.contactTestBitMask = PhysicsCategories.dealine | PhysicsCategories.shieldLine
        physicsBody?.collisionBitMask = PhysicsCategories.none
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setColorFromPalette(colorXPos: CGFloat){
        if (self.scene != nil) {
            let color = (self.scene as! GameScene).colorRamp.getPixelColor(position: CGPoint(x: colorXPos, y: (self.scene as! GameScene).colorRamp.frame.height / 2))
            
            self.circle!.fillColor = color!
            self.color = color!
        }
    }
    
    func go() {
        //self.physicsBody?.applyImpulse(CGVector(dx: 0, dy: -1 * GAME_SPEED_FALL))
        explode()
        let moveEnemyAction = SKAction.moveTo(y: (scene?.frame.minY)! - 20, duration: TimeInterval((scene?.frame)!.maxY/100))
        self.run(
            SKAction.sequence([
                moveEnemyAction,
                SKAction.removeFromParent()
                ])
        )
    }
    
    func explode() {
        if let emitter = SKEmitterNode(fileNamed: "sparks") {
            print("explodein")
            emitter.position = self.position
            emitter.zPosition = 43
            emitter.particleColor = self.circle!.fillColor
            self.circle!.addChild(emitter)
        }
    }
}
