
//
//  Multi.swift
//  Color Killer
//
//  Created by Rafael Seara on 14/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

// TODO; Problem removing the XEnemy. DO NOT USE THIS ENEMY

import SpriteKit

class XEnemy: LittleEnemyNode {
    
    var topLeft, topRight, bottomLeft, bottomRight : NormalLittleEnemy?
    
    init(colors: [UIColor], position: CGPoint, size: CGSize) {
        super.init(image: nil, color: UIColor.clear, position: position, size: size)
        self.name = NodeNames.XEnemy
        self.position = position
        self.physicsBody?.affectedByGravity = false
        
        let posTopLeft = CGPoint(x: -1 * (size.width / 4), y: (size.height / 4))
        let posTopRight = CGPoint(x: (size.width / 4), y: (size.height / 4))
        let posBottomLeft = CGPoint(x: -1 * (size.width / 4), y: -1 * (size.height / 4))
        let posBottomRight = CGPoint(x: (size.width / 4), y: -1 * (size.height / 4))
        
        
        topLeft = NormalLittleEnemy(color: colors[0], position: posTopLeft, size: CGSize(width: size.width / 2, height: size.height / 2))
        topRight = NormalLittleEnemy(color: colors[1], position: posTopRight, size: CGSize(width: size.width / 2, height: size.height / 2))
        bottomLeft = NormalLittleEnemy(color: colors[2], position: posBottomLeft, size: CGSize(width: size.width / 2, height: size.height / 2))
        bottomRight = NormalLittleEnemy(color: colors[3], position: posBottomRight, size: CGSize(width: size.width / 2, height: size.height / 2))
        
        topLeft?.physicsBody?.allowsRotation = true
        topRight?.physicsBody?.allowsRotation = true
        bottomLeft?.physicsBody?.allowsRotation = true
        bottomRight?.physicsBody?.allowsRotation = true

        addChild(topLeft!)
        addChild(topRight!)
        addChild(bottomLeft!)
        addChild(bottomRight!)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func go() {
        let fixedJoint1 = SKPhysicsJointFixed.joint(withBodyA: self.physicsBody!, bodyB: topLeft!.physicsBody!, anchor: self.position)
        
        let fixedJoint2 = SKPhysicsJointFixed.joint(withBodyA: self.physicsBody!, bodyB: topRight!.physicsBody!, anchor: self.position)
        
        let fixedJoint3 = SKPhysicsJointFixed.joint(withBodyA: self.physicsBody!, bodyB: bottomLeft!.physicsBody!, anchor: self.position)
        
        let fixedJoint4 = SKPhysicsJointFixed.joint(withBodyA: self.physicsBody!, bodyB: bottomRight!.physicsBody!, anchor: self.position)
        
        scene!.physicsWorld.add(fixedJoint1)
        scene!.physicsWorld.add(fixedJoint2)
        scene!.physicsWorld.add(fixedJoint3)
        scene!.physicsWorld.add(fixedJoint4)

        self.physicsBody?.applyImpulse(CGVector(dx: 0, dy: -1 * GAME_SPEED_FALL))
    }
    
    func hasChilds() -> Bool {
        return (children.count > 0 )
    }
}

