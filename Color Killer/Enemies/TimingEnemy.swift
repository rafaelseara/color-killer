//
//  TimingEnemy.swift
//  Color Killer
//
//  Created by Rafael Seara on 17/03/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import SpriteKit

class TimingEnemy: LittleEnemyNode {
    
    var colors: [UIColor] = []
    var currentColor = 0
    var changeColorTimer = Timer()
    
    init(colors: [UIColor], position: CGPoint, size: CGSize, time: Int) {
        self.colors = colors
        super.init(image: nil, color: colors[0], position: position, size: size)
        physicsBody?.categoryBitMask = PhysicsCategories.timingEnemy
        name = NodeNames.TimingEnemy

        changeColorTimer = Timer.scheduledTimer(timeInterval: TimeInterval(time), target: self, selector: #selector(changeColor), userInfo: nil, repeats: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func removeFromParent() {
        super .removeFromParent()
        self.changeColorTimer.invalidate()
    }
    
    @objc func changeColor(){
        print("color change", colors.count)
        
        if (currentColor < colors.count - 1) {
            currentColor += 1
            self.color = colors[currentColor]
        } else {
            currentColor = 0
            self.color = colors[currentColor]
        }
    }
}

