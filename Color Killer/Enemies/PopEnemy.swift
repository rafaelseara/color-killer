//
//  PopEnemy.swift
//  Color Killer
//
//  Created by Rafael Seara on 11/10/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import Foundation
import SpriteKit

class PopEnemy: LittleEnemyNode {
    
    var colors: [UIColor] = []
    var nChilds: Int
    var level: Int
    var roundSize: CGSize
    
    init(colors: [UIColor], levels: Int, position: CGPoint, size: CGSize, nChilds: Int) {
        self.colors = colors
        self.nChilds = nChilds
        self.level = levels
        self.roundSize = size
        let randomColorIndx = Int.random(in: 0 ... colors.count - 1)
        print(randomColorIndx)
        super.init(image: nil, color: colors[randomColorIndx], position: position, size: size)
        physicsBody?.categoryBitMask = PhysicsCategories.popEnemy
        name = NodeNames.PopEnemy
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func removeFromParent() {
        super .removeFromParent()
    }
    
    func getNumberChilds() -> Int {
        return self.nChilds
    }
    
    func pop() {
        if (level > 1) {
            let smallerSize = roundSize.height * (2 / 3)
            let leftPopEnemy = PopEnemy(colors: colors, levels: level - 1, position: position, size: CGSize(width: smallerSize, height: smallerSize), nChilds: nChilds)
             let rightPopEnemy = PopEnemy(colors: colors, levels: level - 1, position: position, size: CGSize(width: smallerSize, height: smallerSize), nChilds: nChilds)
            scene!.addChild(leftPopEnemy)
            scene!.addChild(rightPopEnemy)
            applyRandomDir(direction: -1, enemy: leftPopEnemy)
            applyRandomDir(direction: 1, enemy: rightPopEnemy)
            
        } else {
            var direction: CGFloat = -1
            let smallerSize = roundSize.height / 3

            for child in 0...(nChilds-1) {
                let normalEnemy = NormalLittleEnemy(color: colors[getRandomFromColors()], position: position, size: CGSize(width: smallerSize, height: smallerSize))
                parent!.addChild(normalEnemy)
                
                direction = direction * (-1)
                applyRandomDir(direction: direction, enemy: normalEnemy)
            }
        }
    }
    
    private func applyRandomDir(direction: CGFloat, enemy: LittleEnemyNode) {
        let dx = direction * CGFloat.random(in: 5 ... 15)
        let dy =  CGFloat.random(in: 15 ... 20)
    
        enemy.physicsBody?.mass = CGFloat.random(in: 0.3 ... 0.6)
        enemy.physicsBody?.applyImpulse(CGVector(dx: dx , dy: -dy))
        //enemy.physicsBody?.applyAngularImpulse(CGFloat.random(in: 6 ... 50))
    }
    
    private func getRandomFromColors() -> Int {
        return Int.random(in: 0 ... self.colors.count - 1)
    }
    
    private func getColorByLevel() -> Int {
        return Int.random(in: 0 ... self.colors.count - 1)
    }

}

