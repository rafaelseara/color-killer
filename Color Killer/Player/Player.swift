//
//  Player.swift
//  Color Killer
//
//  Created by Rafael Seara on 15/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//
//  Every 5 minutes a player gets a life if their life number is less than 5
//  make note that while he plays the time cannot progress
//

import Foundation

class Player {
    
    private var lives: Int = 3
    private var currentLvl: Int = 1
    private var unlockedItems: [Int] = []
    private var lifeExpiration: Date? = nil


    public func getLifes() -> Int {
        return lives
    }
    
    public func loseLife(times: Int) {
        lives = lives - times
        if (lives <= 0) {
            let date = Date()
            var components = DateComponents()
            //components.setValue(1, for: Calendar.Component.minute)
            components.setValue(10, for: Calendar.Component.second)
            lifeExpiration = Calendar.current.date(byAdding: components, to: date)
        }
    }
    
    public func addLife() {
        lives = lives + 1
        lifeExpiration = nil
    }
    
    public func isDead() -> Bool{
        return lives == 0
    }
    
    public func revive() {
        lives = 3
        lifeExpiration = nil
    }
    
    public func setCurrentLevel(level: Int) {
        currentLvl = level
    }
    
    public func getCurrentLevel() -> Int {
        return currentLvl
    }
    
    public func getLifeTimer() -> Date? {
        return lifeExpiration
    }
    
}
