//
//  GameData.swift
//  Color Killer
//
//  Created by Rafael Seara on 16/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

class GameData {
    static let shared = GameData()
    var player = Player()
    var parser: LvlParser?
    var isPaused: Bool = false
    var scene: GameScene?
    var controller: GameViewController?
    var nightMode : Bool = false
    var levels: Int = 40
    var extraLevels: Int = 30
    
    var currentShots : Int = 10 {
        willSet {
            if ((scene) != nil && newValue < currentShots) {
                print("remove bar")
                scene?.staminaBar.removeBarSegment()
            }
        }
    }
    
    private init() { }
}
