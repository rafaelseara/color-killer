//
//  IceCube.swift
//  Color Killer
//
//  Created by Rafael Seara on 11/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//


import SpriteKit

class IceCube: ArtifactNode {
    
    init(color: UIColor, position: CGPoint, size: CGSize) {
        super.init(image: "IceCube", color: color, position: position, size: size)
        self.setupNode()
    }
    
    var colorXPos: CGFloat = 0

    init(colorXPos: CGFloat, position: CGPoint, size: CGSize) {
        super.init(image: "IceCube", color: .red, position: position, size: size)
        self.colorXPos = colorXPos
        self.setupNode()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupNode() {
        physicsBody?.categoryBitMask = PhysicsCategories.iceCube
        name = NodeNames.IceCube
    }
    
    func froze(scene: SKScene) {
        
        let allNodes = scene.children
        
        for node in allNodes {
            switch node.name {
            case (NodeNames.MeteorEnemy):
                node.isPaused = true
                (node as! MeteorEnemy).removeFire()
                break
            case (NodeNames.NormalEnemy), (NodeNames.MultEnemy):
                node.isPaused = true
                break
            default: continue
            }
        }
    }
}

