//
//  Bomb.swift
//  Color Killer
//
//  Created by Rafael Seara on 12/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class Bomb: ArtifactNode {
    
    init(color: UIColor, position: CGPoint, size: CGSize) {
        super.init(image: "Bomb", color: color, position: position, size: size)
        self.setupNode()
    }
    
    var colorXPos: CGFloat = 0

    init(colorXPos: CGFloat, position: CGPoint, size: CGSize) {
        super.init(image: "Bomb", color: .red, position: position, size: size)
        self.colorXPos = colorXPos
        self.setupNode()
    }
    
    private func setupNode() {
        physicsBody?.categoryBitMask = PhysicsCategories.bomb
        name = NodeNames.Bomb
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func explode(scene: SKScene){
        
        let RADIUS = 3 * self.circle.frame.width
        
        for node in scene.children {
            let distance = distanceBetweenPoints(first: node.position, second: self.position)

            if distance < Float(RADIUS) && (
                node.name == NodeNames.IceCube ||
                node.name == NodeNames.MeteorEnemy ||
                node.name == NodeNames.Bomb ||
                node.name == NodeNames.MultEnemy ||
                node.name == NodeNames.NormalEnemy) {
                
                    node.removeFromParent()
            }
        }
    }
    
    private func distanceBetweenPoints(first: CGPoint, second: CGPoint) -> Float {
        return sqrt(pow(Float(second.x - first.x),2) + pow(Float(second.y - first.y), 2))
    }
}

