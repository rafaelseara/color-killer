//
//  ColorAll.swift
//  Color Killer
//
//  Created by Rafael Seara on 13/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class ColorAll: ArtifactNode {
    
    init(color: UIColor, position: CGPoint, size: CGSize) {
        super.init(image: "ColorChange", color: color, position: position, size: size)
        self.setupNode()
    }
    
    var colorXPos: CGFloat = 0

    init(colorXPos: CGFloat, position: CGPoint, size: CGSize) {
        super.init(image: "ColorChange", color: .red, position: position, size: size)
        self.colorXPos = colorXPos
        self.setupNode()
    }
    
    private func setupNode() {
        physicsBody?.categoryBitMask = PhysicsCategories.colorAll
        name = NodeNames.ColorAll
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func colorAll(scene: SKScene) {
        for node in scene.children {
            switch node {
            case (is ColorAll):
                continue
            case (is LittleEnemyNode):
                (node as! LittleEnemyNode).circle!.fillColor = self.color
                (node as! LittleEnemyNode).color = self.color
                break
            case (is ArtifactNode):
                (node as! ArtifactNode).circle.fillColor = self.color
                (node as! ArtifactNode).color = self.color
                break
            default: continue
            }
        }
    }
}

