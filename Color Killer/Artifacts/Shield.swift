//
//  Shield.swift
//  Color Killer
//
//  Created by Rafael Seara on 13/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class Shield: ArtifactNode {
    
    init(color: UIColor, position: CGPoint, size: CGSize) {
        super.init(image: "Shield", color: color, position: position, size: size)
        self.setupNode()
    }
    
    var colorXPos: CGFloat = 0

    init(colorXPos: CGFloat, position: CGPoint, size: CGSize) {
        super.init(image: "Shield", color: .red, position: position, size: size)
        self.colorXPos = colorXPos
        self.setupNode();
    }
    
    private func setupNode() {
        physicsBody?.categoryBitMask = PhysicsCategories.shield
        name = NodeNames.Shield
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func applyShield(scene: SKScene) {
        
        if ((scene.childNode(withName: NodeNames.ShieldLine) == nil)) {
            let deadline = scene.childNode(withName: NodeNames.Deadline)!
            
            let shieldLine = ShieldLine.init(position: CGPoint(x: deadline.position.x, y: deadline.position.y + 50), size: CGSize(width: deadline.frame.width, height: 4.0))
            
            scene.addChild(shieldLine)
        }
    }
}

class ShieldLine: SKSpriteNode {
    
    init(position: CGPoint, size: CGSize) {
        super.init(texture: nil, color: UIColor.blue, size: size)
        self.position = position
        physicsBody = SKPhysicsBody(rectangleOf: size)
        physicsBody?.isDynamic = false
        name = NodeNames.ShieldLine
        physicsBody?.categoryBitMask = PhysicsCategories.shieldLine
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
