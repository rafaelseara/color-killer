//
//  Stamina.swift
//  Color Killer
//
//  Created by Rafael Seara on 19/10/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import SpriteKit

class Stamina: ArtifactNode {
    
    init(color: UIColor, position: CGPoint, size: CGSize) {
        super.init(image: "FlashBlue", color: .white, position: position, size: size)
        self.setupNode()
    }
    
    var colorXPos: CGFloat = 0
    
    init(colorXPos: CGFloat, position: CGPoint, size: CGSize) {
        super.init(image: "FlashBlue", color: .red, position: position, size: size)
        self.colorXPos = colorXPos
        self.setupNode()
    }
    
    private func setupNode() {
        physicsBody?.categoryBitMask = PhysicsCategories.stamina
        name = NodeNames.Stamina
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addStamina(scene: GameScene){
        scene.staminaBar.activateStaminaMode()
    }
}

