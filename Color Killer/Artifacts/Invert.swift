//
//  Invert.swift
//  Color Killer
//
//  Created by Rafael Seara on 13/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

//TODO: Fix the problem with the inverted bar. Do not use this artifact while its not fixed


import SpriteKit

class Invert: ArtifactNode {
    
    init(color: UIColor, position: CGPoint, size: CGSize) {
        super.init(image: "Invertbar", color: color, position: position, size: size)
        physicsBody?.categoryBitMask = PhysicsCategories.invert
        name = NodeNames.Invert
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func invertNow(scene: SKScene) {
        
        let node = scene.childNode(withName: NodeNames.ColorRamp)!
        
        let colorRamp = (node as! TouchableColorRamp)
        
        let uiimage = UIImage(cgImage: (colorRamp.texture?.cgImage())!)
        
        let rotatedTexture = SKTexture(cgImage: (imageRotatedByDegrees(oldImage: uiimage, deg: 180, node: node as! SKSpriteNode)).cgImage!)
        
        colorRamp.texture = rotatedTexture
        //(scene.childNode(withName: NodeNames.ColorRamp) as! TouchableColorRamp).texture = rotatedImage
    }
    
    func imageRotatedByDegrees(oldImage: UIImage, deg degrees: CGFloat, node: SKSpriteNode) -> UIImage {
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: node.size.width, height: node.size.height))
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat.pi / 180)
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat.pi / 180))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: 1.0)
        bitmap.draw(oldImage.cgImage!, in: CGRect(x: -node.size.width / 2, y: -node.size.height / 2, width: node.size.width, height: node.size.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

