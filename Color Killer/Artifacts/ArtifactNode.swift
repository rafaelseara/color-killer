//
//  ArtifactNode.swift
//  Color Killer
//
//  Created by Rafael Seara on 11/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class ArtifactNode : SKSpriteNode {
    
    enum Error: Swift.Error {
        case imageOrColor
    }
    
    let circle = SKShapeNode(circleOfRadius: 20)

    init(image: String, color: UIColor, position: CGPoint, size: CGSize){
        super.init(texture: nil, color: color, size: .zero)
        
        circle.strokeColor = .white
        circle.glowWidth = 1.0
        circle.fillColor = color
        circle.zPosition = -1
        self.position = position
        addChild(circle)

        let topNodewithImg = SKSpriteNode(texture: SKTexture(imageNamed: image), color: UIColor.clear, size: size)
        print(size)
        print(self.size.width)
        topNodewithImg.zPosition = 1
        topNodewithImg.scale(to: CGSize(width: size.width / 1.5, height: size.height / 1.5))
        //topNodewithImg.colorBlendFactor = 1.0
        addChild(topNodewithImg)
        
        //self.colorBlendFactor = 1.0
        physicsBody = SKPhysicsBody(circleOfRadius: 20)
        physicsBody?.isDynamic = true
        physicsBody?.friction = 0
        physicsBody?.linearDamping = 0
        physicsBody?.affectedByGravity = false
        physicsBody?.allowsRotation = false
        physicsBody?.contactTestBitMask = PhysicsCategories.dealine | PhysicsCategories.shieldLine | PhysicsCategories.waveFire 
        physicsBody?.collisionBitMask = PhysicsCategories.none
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setColorFromPalette(colorXPos: CGFloat){
        if (self.scene != nil) {
            let color = (self.scene as! GameScene).colorRamp.getPixelColor(position: CGPoint(x: colorXPos, y: (self.scene as! GameScene).colorRamp.frame.height / 2))
            
            self.circle.fillColor = color!
        }
    }
    
    func go() {
        
        let moveArtifactAction = SKAction.moveTo(y: (scene?.frame.minY)! - 20, duration: GAME_SPEED_FALL)
        self.run(
            SKAction.sequence([
                moveArtifactAction,
                SKAction.removeFromParent()
                ])
        )
    }
}
