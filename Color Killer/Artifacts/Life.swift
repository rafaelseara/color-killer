//
//  Life.swift
//  Color Killer
//
//  Created by Rafael Seara on 15/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

class Life: ArtifactNode {
    
    init(color: UIColor, position: CGPoint, size: CGSize) {
        super.init(image: "Heart", color: color, position: position, size: size)
        self.setupNode()
    }
    
    var colorXPos: CGFloat = 0
    
    init(colorXPos: CGFloat, position: CGPoint, size: CGSize) {
        super.init(image: "Heart", color: .red, position: position, size: size)
        self.colorXPos = colorXPos
        self.setupNode()
    }
    
    private func setupNode() {
        physicsBody?.categoryBitMask = PhysicsCategories.life
        name = NodeNames.Life
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addLife(scene: GameScene){
        scene.gameData.player.addLife()
        let livesNode = scene.childNode(withName: NodeNames.LivesCounterParent)?.childNode(withName: NodeNames.LivesCounter) as! SKLabelNode
        livesNode.text = String(scene.gameData.player.getLifes())
    }
}

