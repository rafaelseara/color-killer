//
//  Collisions.swift
//  Color Killer
//
//  Created by Rafael Seara on 17/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit
//game collisions
extension GameScene {
    //every contact in the scene use contactTestBitMask
    func didBegin(_ contact: SKPhysicsContact) {
        
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask

        if (contact.bodyA.node != nil && contact.bodyB.node != nil) {
            switch contactMask {
            case (PhysicsCategories.shieldLine | PhysicsCategories.normalEnemy) ,
                 (PhysicsCategories.shieldLine | PhysicsCategories.multEnemy),
                 (PhysicsCategories.shieldLine | PhysicsCategories.meteorEnemy),
                 (PhysicsCategories.shieldLine | PhysicsCategories.timingEnemy):
                
                Shield_vs_Enemy(contact: contact)
                
            case (PhysicsCategories.dealine | PhysicsCategories.normalEnemy) ,
                 (PhysicsCategories.dealine | PhysicsCategories.multEnemy),
                 (PhysicsCategories.dealine | PhysicsCategories.meteorEnemy),
                 (PhysicsCategories.dealine | PhysicsCategories.timingEnemy),
                 (PhysicsCategories.dealine | PhysicsCategories.popEnemy):
                
                Deadline_vs_Enemy(contact: contact)
                
            case (PhysicsCategories.dealine | PhysicsCategories.iceCube),
                 (PhysicsCategories.dealine | PhysicsCategories.bomb),
                 (PhysicsCategories.dealine | PhysicsCategories.colorAll),
                 (PhysicsCategories.dealine | PhysicsCategories.shield),
                 (PhysicsCategories.dealine | PhysicsCategories.invert),
                 (PhysicsCategories.dealine | PhysicsCategories.life),
                 (PhysicsCategories.dealine | PhysicsCategories.stamina):
                
                Deadline_vs_Artifact(contact: contact)
                
            case (PhysicsCategories.waveFire | PhysicsCategories.normalEnemy),
                 (PhysicsCategories.waveFire | PhysicsCategories.multEnemy),
                 (PhysicsCategories.waveFire | PhysicsCategories.meteorEnemy),
                 (PhysicsCategories.waveFire | PhysicsCategories.timingEnemy),
                 (PhysicsCategories.waveFire | PhysicsCategories.popEnemy):
                
                if isColorHit(waveNode: contact.bodyA.node as! SKSpriteNode, droppingNode: contact.bodyB.node as! SKSpriteNode) {
                    
                    Wave_vs_Enemy(contact: contact)
                }
            case (PhysicsCategories.waveFire | PhysicsCategories.iceCube),
                 (PhysicsCategories.waveFire | PhysicsCategories.bomb),
                 (PhysicsCategories.waveFire | PhysicsCategories.colorAll),
                 (PhysicsCategories.waveFire | PhysicsCategories.shield),
                 (PhysicsCategories.waveFire | PhysicsCategories.invert),
                 (PhysicsCategories.waveFire | PhysicsCategories.life),
                 (PhysicsCategories.waveFire | PhysicsCategories.stamina):
                
                if isColorHit(waveNode: contact.bodyA.node as! SKSpriteNode, droppingNode: contact.bodyB.node as! SKSpriteNode) {
                    
                    Wave_vs_Artifact(contact: contact)
                }
                
            case (PhysicsCategories.roof | PhysicsCategories.waveFire):
                
                Wave_vs_Roof(contact: contact)
            default:
                print("what contact?")
            }
        }
        
    }
    
    func isColorHit(waveNode: SKSpriteNode, droppingNode: SKSpriteNode) -> Bool {
        var droppingNodeRed : CGFloat = 0
        var droppingNodeGreen : CGFloat = 0
        var droppingNodeBlue : CGFloat = 0
        var droppingNodeAlpha: CGFloat = 0
        droppingNode.color.getRed(&droppingNodeRed, green: &droppingNodeGreen, blue: &droppingNodeBlue, alpha: &droppingNodeAlpha)
        
        var waveRed : CGFloat = 0
        var waveGreen : CGFloat = 0
        var waveBlue : CGFloat = 0
        var waveAlpha: CGFloat = 0
        waveNode.color.getRed(&waveRed, green: &waveGreen, blue: &waveBlue, alpha: &waveAlpha)
        
        //eucleadean color distance
        let powRed = pow((droppingNodeRed - waveRed), 2)
        let powGreen = pow((droppingNodeGreen - waveGreen), 2)
        let powBlue = pow((droppingNodeBlue - waveBlue), 2)
        let colorDistance : CGFloat = powRed + powGreen + powBlue
        
        return (colorDistance < CGFloat(COLOR_TRESHOLD))
    }
    
    func Shield_vs_Enemy (contact: SKPhysicsContact) {
        contact.bodyB.node?.removeFromParent()
        contact.bodyA.node?.removeFromParent()
    }
    
    func Deadline_vs_Enemy (contact: SKPhysicsContact) {
        if contact.bodyA.node?.name == NodeNames.Deadline {
            if contact.bodyB.node?.name == NodeNames.MultEnemy {
                let enemyNode = (contact.bodyB.node as? MultipleHitsEnemy)!
                playerGotHit(hits: enemyNode.getHitsToDie())
            } else {
                playerGotHit(hits: 1)
            }
            contact.bodyB.node?.removeFromParent()
        }else{
            if contact.bodyA.node?.name == NodeNames.MultEnemy {
                let enemyNode = (contact.bodyA.node as? MultipleHitsEnemy)!
                playerGotHit(hits: enemyNode.getHitsToDie())
            } else {
                playerGotHit(hits: 1)
            }
            contact.bodyA.node?.removeFromParent()
        }
    }
    
    func Deadline_vs_Artifact(contact: SKPhysicsContact) {
        //artifacts touching deadline
        // no problem touching deadline
        if contact.bodyA.node?.name == NodeNames.Deadline {
            contact.bodyB.node?.removeFromParent()
        }else{
            contact.bodyA.node?.removeFromParent()
        }
    }
    
    func Wave_vs_Enemy(contact: SKPhysicsContact) {
        //shot touched enemy
        //get color of enemy
        
        print("enemyhit")
        
        var enemyNode : SKSpriteNode
        var waveNode : SKSpriteNode
        
        switch (contact.bodyA.node?.name) {
        case NodeNames.NormalEnemy:
            enemyNode = (contact.bodyA.node as? NormalLittleEnemy)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.MultEnemy:
            enemyNode = (contact.bodyA.node as? MultipleHitsEnemy)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.MeteorEnemy:
            enemyNode = (contact.bodyA.node as? MeteorEnemy)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.TimingEnemy:
            enemyNode = (contact.bodyA.node as? TimingEnemy)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.PopEnemy:
            enemyNode = (contact.bodyA.node as? PopEnemy)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.WaveFire:
            waveNode = (contact.bodyA.node as? SKSpriteNode)!
            switch(contact.bodyB.node?.name) {
            case NodeNames.NormalEnemy:
                enemyNode = (contact.bodyB.node as? NormalLittleEnemy)!
            case NodeNames.MultEnemy:
                enemyNode = (contact.bodyB.node as? MultipleHitsEnemy)!
            case NodeNames.MeteorEnemy:
                enemyNode = (contact.bodyB.node as? MeteorEnemy)!
            case NodeNames.TimingEnemy:
                enemyNode = (contact.bodyB.node as? TimingEnemy)!
            case NodeNames.PopEnemy:
                enemyNode = (contact.bodyB.node as? PopEnemy)!
            default:
                fatalError("ERROR 4")
            }
        default:
            fatalError("ERROR 3")
        }
        
        switch enemyNode {
            case (is NormalLittleEnemy),
                 (is MeteorEnemy),
                 (is TimingEnemy):
                enemyNode.removeFromParent()
                waveNode.removeFromParent()
            case is MultipleHitsEnemy:
                (enemyNode as! MultipleHitsEnemy).hit()
                waveNode.removeFromParent()
            case is PopEnemy:
                (enemyNode as! PopEnemy).pop()
                enemyNode.removeFromParent()
                waveNode.removeFromParent()
            default:
                fatalError("ERROR Bad Enemy")
            }
        
        self.run(SKAction.playSoundFileNamed("lip", waitForCompletion: false))
    }
    
    func Wave_vs_Roof(contact: SKPhysicsContact) {
        if let wave = contact.bodyA.node?.name == NodeNames.WaveFire ?
            contact.bodyA.node as? SKSpriteNode : contact.bodyB.node as? SKSpriteNode {
            wave.removeFromParent()
        }
    }
    
    func Wave_vs_Artifact(contact: SKPhysicsContact) {
        
        var artifactNode : SKSpriteNode
        var waveNode : SKSpriteNode
        
        switch (contact.bodyA.node?.name) {
        case NodeNames.IceCube:
            artifactNode = (contact.bodyA.node as? ArtifactNode)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.Bomb:
            artifactNode = (contact.bodyA.node as? Bomb)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.ColorAll:
            artifactNode = (contact.bodyA.node as? ColorAll)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.Shield:
            artifactNode = (contact.bodyA.node as? Shield)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.Invert:
            artifactNode = (contact.bodyA.node as? Invert)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.Life:
            artifactNode = (contact.bodyA.node as? Life)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.Stamina:
            artifactNode = (contact.bodyA.node as? Stamina)!
            waveNode = (contact.bodyB.node as? SKSpriteNode)!
        case NodeNames.WaveFire:
            waveNode = (contact.bodyA.node as? SKSpriteNode)!
            switch(contact.bodyB.node?.name) {
            case NodeNames.IceCube:
                artifactNode = (contact.bodyB.node as? IceCube)!
            case NodeNames.Bomb:
                artifactNode = (contact.bodyB.node as? Bomb)!
            case NodeNames.ColorAll:
                artifactNode = (contact.bodyB.node as? ColorAll)!
            case NodeNames.Shield:
                artifactNode = (contact.bodyB.node as? Shield)!
            case NodeNames.Invert:
                artifactNode = (contact.bodyB.node as? Invert)!
            case NodeNames.Life:
                artifactNode = (contact.bodyB.node as? Life)!
            case NodeNames.Stamina:
                artifactNode = (contact.bodyB.node as? Stamina)!
            default:
                fatalError("ERROR 2")
            }
        default:
            fatalError("ERROR 1")
        }
        
        
        switch artifactNode {
        case is IceCube:
            artifactNode.removeFromParent()
            waveNode.removeFromParent()
            //froze all in scene
            (artifactNode as! IceCube).froze(scene: self)
        case is Bomb:
            artifactNode.removeFromParent()
            waveNode.removeFromParent()
            
            (artifactNode as! Bomb).explode(scene: self)
        case is ColorAll:
            artifactNode.removeFromParent()
            waveNode.removeFromParent()
            
            (artifactNode as! ColorAll).colorAll(scene: self)
        case is Shield:
            artifactNode.removeFromParent()
            waveNode.removeFromParent()
            
            (artifactNode as! Shield).applyShield(scene: self)
        case is Invert:
            artifactNode.removeFromParent()
            waveNode.removeFromParent()
            
            (artifactNode as! Invert).invertNow(scene: self)
        case is Life:
            artifactNode.removeFromParent()
            waveNode.removeFromParent()
            
            (artifactNode as! Life).addLife(scene: self)
        case is Stamina:
            artifactNode.removeFromParent()
            waveNode.removeFromParent()
            
            (artifactNode as! Stamina).addStamina(scene: self)
        default:
            print("bad artifact")
        }
        self.run(SKAction.playSoundFileNamed("lip", waitForCompletion: false))
    }
    
}
