//
//  UtilsGameScene.swift
//  Color Killer
//
//  Created by Rafael Seara on 30/03/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import Foundation
import SpriteKit


func positionsToColors(positions: [CGPoint], colorRamp: TouchableColorRamp) -> [UIColor] {
    var colors: [UIColor] = []
    positions.forEach { (pos) in
        colors.append(colorRamp.getPixelColor(position: pos)!)
    }
    return colors
}

func generateMultipleColors(times: Int, colorRamp: TouchableColorRamp) -> [UIColor] {
    var colors: [UIColor] = []
    for _ in 1...times {
        let colorX = arc4random_uniform(UInt32(colorRamp.frame.maxX))
        let minColorY : UInt32 = UInt32(colorRamp.frame.minY)
        let maxColorY : UInt32 = UInt32((colorRamp.frame.maxY - colorRamp.frame.minY))
        let colorY = arc4random_uniform(maxColorY - minColorY) + minColorY
        colors.append(colorRamp.getPixelColor(position: CGPoint(x: Int(colorX), y: Int(colorY)))!)
    }
    return colors
}
