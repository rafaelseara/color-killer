//
//  Enums.swift
//  Color Killer
//
//  Created by Rafael Seara on 11/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import UIKit

let DAY_COLOR = (226/255, 226/255, 144/255, 255/255)
let NIGHT_COLOR = (41/255, 45/255, 99/255, 255/255)


//ColorConstants.swift
struct AppColor {
    
    struct Night {
        //static let Background = UIColor(red: 41/255, green: 45/255, blue: 99/255, alpha: 255/255)
        static let Background = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 255/255)
    }
    
    struct Day {
//        static let Background = UIColor(red: 226/255, green:  226/255, blue:  144/255, alpha:  255)
        static let Background = UIColor.systemYellow
    }

//    private struct Alphas {
//        static let Opaque = CGFloat(1)
//        static let SemiOpaque = CGFloat(0.8)
//        static let SemiTransparent = CGFloat(0.5)
//        static let Transparent = CGFloat(0.3)
//    }
//
//    static let appPrimaryColor =  UIColor.white.withAlphaComponent(Alphas.SemiOpaque)
//    static let appSecondaryColor =  UIColor.blue.withAlphaComponent(Alphas.Opaque)
//
//    struct TextColors {
//        static let Error = AppColor.appSecondaryColor
//        static let Success = UIColor(red: 0.1303, green: 0.9915, blue: 0.0233, alpha: Alphas.Opaque)
//    }
//
//    struct TabBarColors{
//        static let Selected = UIColor.white
//        static let NotSelected = UIColor.black
//    }
//
//    struct OverlayColor {
//        static let SemiTransparentBlack = UIColor.black.withAlphaComponent(Alphas.Transparent)
//        static let SemiOpaque = UIColor.black.withAlphaComponent(Alphas.SemiOpaque)
//        static let demoOverlay = UIColor.black.withAlphaComponent(0.6)
//    }
}

let GAME_SPEED_CALL = Double(GameData.shared.parser != nil ? GameData.shared.parser!.getSpeedCall() : 30)
let GAME_SPEED_FALL = Double(GameData.shared.parser != nil ? GameData.shared.parser!.getSpeedFall() : 8)
let COLOR_TRESHOLD = Double(GameData.shared.parser != nil ? GameData.shared.parser!.getColorTresh() : 0.1)
let WAVE_SPEED = 2
let COLUMN_DIVISION = 10.0
//let DEFAULT_BG_COLOR = (37/255, 45/255, 67/255, 1.0)

let GAME_FONT = "MarkerFelt-Thin" //"Optima-ExtraBlack"
let GAME_FONT_BOLD = "MarkerFelt-Wide"

let COUNTER_FONT = "Courier"

enum GameModes: Int {
    case Random = 1, Quest, Test, Enemies
    var name: String {
        get { return String(describing: self) }
    }
}

enum PhysicsCategories {
    static let none : UInt32 = 0
    static let dealine : UInt32 = 0x1
    static var roof : UInt32 = 0x1 << 3
    static var shieldLine : UInt32 = 0x1 << 10

    static var waveFire : UInt32 = 0x1 << 2

    static var normalEnemy : UInt32 = 0x1 << 1
    static var multEnemy : UInt32 = 0x1 << 4
    static var meteorEnemy : UInt32 = 0x1 << 6
    static var timingEnemy : UInt32 = 0x1 << 13
    static var popEnemy : UInt32 = 0x1 << 14
    
    static var iceCube : UInt32 = 0x1 << 5
    static var bomb : UInt32 = 0x1 << 7
    static var colorAll : UInt32 = 0x1 << 8
    static var shield : UInt32 = 0x1 << 9
    static var invert : UInt32 = 0x1 << 11
    static var life : UInt32 = 0x1 << 12
    static var stamina : UInt32 = 0x1 << 15

}

enum NodeNames {
    static let LivesCounter : String = "LivesCounter"
    static let LivesCounterParent : String = "LivesCounterParent"
    static let ColorRamp : String = "ColorRamp"
    static let StaminaBar : String = "StaminaBar"
    static let Deadline : String = "Deadline"
    static let Roof : String = "Roof"
    static let ShieldLine : String = "ShieldLine"

    static let WaveFire : String = "Wave"

    static let NormalEnemy : String = "NormalEnemy"
    static let MultEnemy : String = "MultEnemy"
    static let MeteorEnemy : String = "MeteorEnemy"
    static let XEnemy : String = "XEnemy"
    static let TimingEnemy: String = "TimingEnemy"
    static let PopEnemy: String = "PopEnemy"
    
    static let IceCube : String = "IceCube"
    static let Bomb : String = "Bomb"
    static let ColorAll : String = "ColorAll"
    static let Shield : String = "Shield"
    static let Invert : String = "Invert"
    static let Life : String = "Life"
    static let Stamina : String = "Stamina"

}
