//
//  PausableTimer.swift
//  Color Killer
//
//  Created by Rafael Seara on 16/06/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import Foundation

class PauseableTimer: NSObject {
    var timer: Timer!
    private(set) var isPause: Bool = false
    private var timeLeft: TimeInterval?
    
    init(timer aTimer: Timer) {
        self.timer = aTimer
    }
    
    func pause() {
        if !isPause {
            timeLeft = self.timer.fireDate.timeIntervalSinceNow
            
            self.timer.fireDate = Date(timeIntervalSinceNow: 3600*10000)
            
            isPause = true
        }
    }
    
    func resume() {
        if isPause {
            self.timer.fireDate = Date().addingTimeInterval(timeLeft!)
            isPause = false
        }
    }
    
    func invalidate() {
        self.timer.invalidate()
    }
    
    func restart() {
        pause()
        timeLeft = timer.timeInterval
        resume()
    }
}
