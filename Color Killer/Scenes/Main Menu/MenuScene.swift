//
//  MenuScene.swift
//  Color Killer
//
//  Created by Rafael Seara on 09/02/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import SpriteKit

class MenuScene: SKScene {
    
    let gameData = GameData.shared
    var fallingPausableTimer: PauseableTimer!
    var fallingPausableTimer2: PauseableTimer!
    weak var adManagerDelegate: AdManagerDelegate?
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appBecameActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        fallingNodes()
        displayUI()
    }
    override func didMove(to view: SKView) {
        print("Menu going in")
        adManagerDelegate?.showMenuBanner()
    }
    
    override func willMove(from view: SKView) {
        print("Menu going out")

        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func removeFromParent() {
        fallingPausableTimer.invalidate()
        fallingPausableTimer2.invalidate()
    }
    
    @objc func appMovedToBackground() {
        print("moved to background")
        fallingPausableTimer.pause()
        fallingPausableTimer2.pause()
    }
    
    @objc func appBecameActive() {
        print("moved to Active")
        fallingPausableTimer.resume()
        fallingPausableTimer2.resume()
    }
    
    private func fallingNodes() {

        let fallingTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(spawnNode), userInfo: nil, repeats: true)
        let fallingTimer2 = Timer.scheduledTimer(timeInterval: 1.3, target: self, selector: #selector(spawnNode), userInfo: nil, repeats: true)
        
        fallingPausableTimer = PauseableTimer(timer: fallingTimer)
        fallingPausableTimer2 = PauseableTimer(timer: fallingTimer2)
    }
    
    @objc private func spawnNode() {
        let circle = SKShapeNode(circleOfRadius: 20 )
        
        let minValueX : UInt32 = 20
        let maxValueX : UInt32 = UInt32(frame.width) - 20
        let randomX = arc4random_uniform(maxValueX - minValueX) + minValueX
        
        circle.position = CGPoint(x: CGFloat(randomX), y: frame.maxY)
        circle.strokeColor = .clear
        circle.glowWidth = 1.0
        circle.fillColor = .random
        circle.zPosition = -2
        self.addChild(circle)
        
        let movePlayerAction = SKAction.moveTo(y: (scene?.frame.minY)! - 20, duration: .random(in: 3...6))
        circle.run(
            SKAction.sequence([
                movePlayerAction,
                SKAction.removeFromParent()
                ])
        )
    }
    
    private func displayUI() {
        // background color depending on the mode day/night
        nightMode()
        displayGameTitle()
        
        displayButton(gameMode: GameModes.Test, position: CGPoint(x: self.frame.midX,y: self.frame.midY / 6 * 3 ))
        displayButton(gameMode: GameModes.Random, position: CGPoint(x: self.frame.midX,y: self.frame.midY / 6 * 4 ))
        displayButton(gameMode: GameModes.Quest, position: CGPoint(x: self.frame.midX,y: self.frame.midY / 6 * 5))
        displayButton(gameMode: GameModes.Enemies, position: CGPoint(x: self.frame.midX,y: self.frame.midY / 6 * 6))
        
        // displayCredits()
        displaySettings()
    }
    
    private func nightMode() {
        if(gameData.nightMode) {
            self.backgroundColor = AppColor.Day.Background
        } else {
            self.backgroundColor = AppColor.Night.Background
        }
    }
    
    private func displayGameTitle() {
        let titleLabel = SKLabelNode(fontNamed: GAME_FONT_BOLD)
        titleLabel.text = "COLOR KILLER"
        titleLabel.fontSize = 50.0
        titleLabel.name = "title"
        titleLabel.fontColor = gameData.nightMode ? AppColor.Night.Background : AppColor.Day.Background
        titleLabel.position = CGPoint(x: self.frame.midX, y: (self.frame.maxY - self.frame.midY / 2))
        addChild(titleLabel)
        shakeLabel(label: titleLabel)
    }
    
    private func shakeLabel(label: SKLabelNode) {
        let scaleIn = SKAction.scale(to: 0.9, duration: 0.8)
        let scaleOut = SKAction.scale(to: 1.0, duration: 0.8)
        let pulseGroup = SKAction.sequence([scaleIn, scaleOut])
        let repeatSequence = SKAction.repeatForever(pulseGroup)
        label.run(repeatSequence)
    }
    
    
    
    @objc private func startGame(sender: StrokeButtonNode) {
        print("start game")
        let gameScene = GameScene(size: (sender.scene?.size)!)
        gameScene.adManagerDelegate = adManagerDelegate
        switch sender.name {
        case GameModes.Quest.name:
            gameScene.selectedMode = GameModes.Quest
            goToGameScene(gameScene: gameScene)
            break
        case GameModes.Random.name:
            gameScene.selectedMode = GameModes.Random
            goToGameScene(gameScene: gameScene)
            break
        case GameModes.Test.name:
            gameScene.selectedMode = GameModes.Test
            let LevelsMenuScene = UnlockScene(size: (self.scene?.size)!)
            let transition = SKTransition.push(with: .left, duration: 0.3)
            self.view?.presentScene(LevelsMenuScene, transition: transition)
            break
        case GameModes.Enemies.name:
            gameScene.selectedMode = GameModes.Enemies
            goToGameScene(gameScene: gameScene)
            break
        default:
            print("Error selecting game mode")
        }
    }
    
    private func goToGameScene(gameScene: GameScene) {
        if (gameData.player.getLifeTimer() != nil && gameData.player.getLifeTimer()! > Date()) {
            displayCountdownCard()
        } else {
            // gameScene.gameData.player.revive()
            adManagerDelegate?.hideMenuBanner()
            let transition = SKTransition.fade(withDuration: 0.5)
            self.view?.presentScene(gameScene, transition: transition)
        }
    }
    
    private func displayCountdownCard() {
        let transition = SKTransition.fade(withDuration: 0.5)
        let gameOverScene = GameOverScene(size: self.size)
        gameOverScene.text = "No more lives"
        self.view?.presentScene(gameOverScene, transition: transition)
    }
    
    private func displayCredits() {
        let creditsLabel = SKLabelNode(fontNamed: GAME_FONT)
        creditsLabel.text = "Created by Rafael Seara"
        creditsLabel.fontSize = 10
        creditsLabel.fontColor = SKColor.gray
        creditsLabel.position = CGPoint(x: self.frame.midX, y: self.frame.minY + 50)
        addChild(creditsLabel)
    }
    
    private func displaySettings() {
        let settingsButton = SettingsNode(size: CGSize(width: 30, height: 30))
        settingsButton.position = CGPoint(x: self.frame.maxX - 50, y: self.frame.maxY - 50)
        self.addChild(settingsButton)
    }
    
    
    private func displayButton(gameMode: GameModes, position: CGPoint) {
        //let startButtonTexture: SKTexture! = SKTexture(imageNamed: "button")
        //let startButtonTextureSelected: SKTexture! = SKTexture(imageNamed: "buttonSelected.png")
        //let button = FTButtonNode(normalTexture: startButtonTexture, selectedTexture: startButtonTextureSelected, disabledTexture: startButtonTexture)
        
        let button = StrokeButtonNode(normalColor: gameData.nightMode ? AppColor.Night.Background : AppColor.Day.Background, selectedColor: .white, disabledColor: .gray, size: CGSize(width: self.frame.maxX / 3, height: 40), cornerRadius: 20)
        button.setButtonLabel(title: gameMode.name as NSString, font: GAME_FONT, fontSize: 20)
        button.setButtonAction(target: self, triggerEvent: .TouchUpInside, action: #selector(startGame(sender:)))
        button.position = position
        button.zPosition = 1
        button.name = gameMode.name
        
        self.addChild(button)
        
        let randomDur: Double = Double.random(in: 0.5 ... 0.7)
        let scaleIn = SKAction.scale(to: 0.9, duration: randomDur)
        let scaleOut = SKAction.scale(to: 1.0, duration: randomDur)
        let pulseGroup = SKAction.sequence([scaleIn, scaleOut])
        let repeatSequence = SKAction.repeatForever(pulseGroup)
        
        button.run(repeatSequence)
        
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0.3...1),
                       green: .random(in: 0.3...1),
                       blue: .random(in: 0.3...1),
                       alpha: 1.0)
    }
}
