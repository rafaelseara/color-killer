//
//  LevelsMenu.swift
//  Color Killer
//
//  Created by Rafael Seara on 04/08/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import SpriteKit

class LevelsMenu: SKScene {
    
    let gameData = GameData.shared
    let currentLevel = 4
    let totalLevels = 10
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        // setTable(size: size)
    }
    
    var scView:UIScrollView!
    let buttonPadding:CGFloat = 20
    var xOffset:CGFloat = 20
    
    override func didMove(to view: SKView) {
        nightMode()
        scView = UIScrollView(frame: decreaseRect(rect: frame, byPercentage: 0.35))
        view.addSubview(scView)
        
        scView.showsHorizontalScrollIndicator = false
        scView.backgroundColor = .clear
        scView.translatesAutoresizingMaskIntoConstraints = false
        
        var totalXOffset:CGFloat = xOffset

        for levelCardN in 1 ... totalLevels {
            let button = UIButton()
            button.tag = levelCardN
            setButtonCardColors(button: button, currentLevel: currentLevel, level: levelCardN)
            
            
            //button.addTarget(self, action: #selector(btnTouch), for: UIControlEvents.touchUpInside)
            
            button.frame = CGRect(x: totalXOffset, y: buttonPadding, width: frame.width / 3, height: scView.frame.height - buttonPadding * 3)
            
            totalXOffset = totalXOffset + CGFloat(buttonPadding) + button.frame.size.width
            scView.addSubview(button)
        }
        
        scView.contentSize = CGSize(width: totalXOffset, height: scView.frame.height)
        setNextLevelOffset(currentLevel: currentLevel, numLevels: totalLevels)
        displayGoToCurrent()
        displayBackButton()
    }
    
    private func displayGoToCurrent() {
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.clipsToBounds = true
        button.setTitle("Current level", for: .normal)
        button.setTitleColor(.white, for: .selected)
        button.setTitleColor(.yellow, for: .normal)
        button.showsTouchWhenHighlighted = true
        button.titleLabel?.font = button.titleLabel?.font.withSize(20)
        button.titleLabel?.textAlignment = .center
        button.frame = CGRect(x: (view?.frame.midX)! - (view?.frame.width)! / 2, y: (view?.frame.maxY)! - 100, width: (view?.frame.width)!, height: 30)
        button.addTarget(self, action: #selector(goToCurrentLvlOffset), for: .touchUpInside)
        view!.addSubview(button)
    }

    @objc private func goToCurrentLvlOffset() {
        setNextLevelOffset(currentLevel: currentLevel, numLevels: totalLevels)
    }
    
    private func setNextLevelOffset(currentLevel: Int, numLevels: Int) {
        let cellWidth = frame.width / 3
        let xOffsetLevelToGo = (cellWidth + xOffset) * CGFloat(currentLevel - 2) + xOffset * 2
        if (xOffsetLevelToGo > 0) {
            scView.setContentOffset(CGPoint(x: xOffsetLevelToGo, y: 0), animated: true)
        } else {
            scView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }

    private func setButtonCardColors(button: UIButton,currentLevel: Int, level: Int) {
        button.backgroundColor = UIColor.darkGray
        button.layer.cornerRadius = 10; // this value vary as per your desire
        button.clipsToBounds = true
        button.layer.borderWidth = 4
        button.setBackgroundImage(UIImage(named: "bg_vertical_\(level)"), for: .normal)
        button.setTitle("\(level)", for: .normal)
        button.titleLabel?.font = button.titleLabel?.font.withSize(40)
        button.titleLabel?.textAlignment = .center
        
        if (level < currentLevel) {
            button.layer.borderColor =  UIColor.green.cgColor
        } else if (level == currentLevel) {
            button.layer.borderColor =  UIColor.yellow.cgColor
            pulsate(button: button)
        } else {
            button.layer.borderColor =  UIColor.gray.cgColor
            button.alpha = 0.3
        }
    }
    
    private func pulsate(button: UIButton) {
        let pulse = CABasicAnimation(keyPath: "transform.scale")
        pulse.duration = 0.7
        pulse.fromValue = 0.97
        pulse.toValue = 1.07
        pulse.autoreverses = true
        pulse.repeatCount = .infinity
        // pulse.initialVelocity = 0.5
        // pulse.damping = 1.0
        button.layer.add(pulse, forKey: nil)
    }
    
    private func displayBackButton() {
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.clipsToBounds = true
        button.setTitle("Back", for: .normal)
        button.setTitleColor(.white, for: .selected)
        button.setTitleColor(.yellow, for: .normal)
        button.showsTouchWhenHighlighted = false
        button.titleLabel?.font = button.titleLabel?.font.withSize(20)
        button.titleLabel?.textAlignment = .center
        button.frame = CGRect(x: 10, y: ((view?.frame.minY)!) + 10, width: 50, height: 50)
        button.addTarget(self, action: #selector(backToMenu), for: .touchUpInside)
        view!.addSubview(button)
    }
    
    @objc private func backToMenu() {
        // let transition = SKTransition.fade(withDuration: 0.5)
        let transition = SKTransition.push(with: .right, duration: 0.5)
        let menuScene = MenuScene(size: self.size)
        view?.subviews.forEach({ (UIView) in
            UIView.removeFromSuperview()
        })
        self.view?.presentScene(menuScene, transition: transition)
    }
    
    private func decreaseRect(rect: CGRect, byPercentage percentage: CGFloat) -> CGRect {
        let startWidth = rect.width
        let startHeight = rect.height
        let adjustmentWidth = (startWidth * 0) / 2.0
        let adjustmentHeight = (startHeight * percentage) / 2.0
        return rect.insetBy(dx: adjustmentWidth, dy: adjustmentHeight)
    }
    
    private func nightMode() {
        if(gameData.nightMode) {
            self.backgroundColor = AppColor.Day.Background
        } else {
            self.backgroundColor = AppColor.Night.Background
        }
    }
}
