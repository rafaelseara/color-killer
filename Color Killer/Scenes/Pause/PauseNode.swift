//
//  PauseNode.swift
//  Color Killer
//
//  Created by Rafael Seara on 14/06/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import Foundation
import SpriteKit

class PauseNode {
    
    let node: SKShapeNode!
    let gameData = GameData.shared
    var gameScene: GameScene
    let rect: CGRect
    let DIVHEIGHT: CGFloat
    let divisions = 5
    
    init(rect: CGRect, cornerRadius: CGFloat, scene: GameScene) {
        self.gameScene = scene
        self.rect = rect
        node = SKShapeNode(rect: rect, cornerRadius: cornerRadius)
        node.name = "pause"
        // create oppacity
        node.fillColor = .black
        node.zPosition = 10
        
        // max 5 divisions
        self.DIVHEIGHT = (rect.maxY - rect.minY) / CGFloat(self.divisions)

        // show pause label
        displayPauseLabel(position: 4)
        
        // show lifes count
        displayLifesCount(position: 3)
        
        // show continue
        displayContinueButton()
        
        // show menu
        displayMenuButton()
    }
    
    func getPauseNode() -> SKShapeNode {
        return node
    }
    
    func displayPauseLabel(position: Int) {
        let pauseLabel = SKLabelNode(fontNamed: GAME_FONT)
        pauseLabel.text = "Paused"
        pauseLabel.fontSize = 25
        pauseLabel.fontColor = SKColor.white
        pauseLabel.position = CGPoint(x: rect.midX, y: rect.minY + DIVHEIGHT * CGFloat(position))
        node.addChild(pauseLabel)
    }
    
    func displayLifesCount(position: Int) {
        let lifesLabel = SKLabelNode(fontNamed: GAME_FONT)
        lifesLabel.fontSize = 15
        let lives = gameData.player.getLifes()
        if (lives > 1) {
            lifesLabel.text = "You have \(lives) lives remaining"
            lifesLabel.fontColor = SKColor.white
        } else {
            lifesLabel.text = "You have \(lives) live remaining"
            lifesLabel.fontColor = .red
        }
        lifesLabel.position = CGPoint(x: rect.midX, y: rect.minY + DIVHEIGHT * CGFloat(position))
        node.addChild(lifesLabel)
    }
    
    func displayContinueButton() {
        let startButtonTexture: SKTexture! = SKTexture(imageNamed: "button")
        let startButtonTextureSelected: SKTexture! = SKTexture(imageNamed: "buttonSelected.png")
        
        let continueBtn = FTButtonNode(normalTexture: startButtonTexture, selectedTexture: startButtonTextureSelected, disabledTexture: startButtonTexture, size: CGSize(width: 100, height: 30))
        continueBtn.setButtonLabel(title: "Continue", font: GAME_FONT, fontSize: 20)
        continueBtn.setButtonAction(target: gameScene, triggerEvent: .TouchUpInside, action: #selector(gameScene.resumeGame))
        continueBtn.position = CGPoint(x: rect.midX - rect.midX / 3 , y: rect.minY + DIVHEIGHT )
        continueBtn.zPosition = 1
        continueBtn.name = "continue"
        node.addChild(continueBtn)
    }
    
    func displayMenuButton() {
        let startButtonTexture: SKTexture! = SKTexture(imageNamed: "button")
        let startButtonTextureSelected: SKTexture! = SKTexture(imageNamed: "buttonSelected.png")
        
        let menuBtn = FTButtonNode(normalTexture: startButtonTexture, selectedTexture: startButtonTextureSelected, disabledTexture: startButtonTexture, size: CGSize(width: 100, height: 30))
        menuBtn.setButtonLabel(title: "Menu", font: GAME_FONT, fontSize: 20)
        menuBtn.setButtonAction(target: gameScene, triggerEvent: .TouchUpInside, action: #selector(gameScene.backToMenu))
        menuBtn.position = CGPoint(x: rect.midX + rect.midX / 3, y: rect.minY + DIVHEIGHT )
        menuBtn.zPosition = 1
        menuBtn.name = "menu"
        node.addChild(menuBtn)
    }
}
