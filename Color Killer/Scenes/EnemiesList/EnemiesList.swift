//
//  EnemiesList.swift
//  Color Killer
//
//  Created by Rafael Seara on 07/06/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import SpriteKit

class EnemiesListScene: SKScene {
    
    let gameData = GameData.shared
    
    override func didMove(to view: SKView) {
        view.backgroundColor = .red
    }
    
    override func removeFromParent() {
    }
    
}
