//
//  GameScene.swift
//  Color Killer
//
//  Created by Rafael Seara on 05/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit
import AudioToolbox

class GameScene: SKScene, SKPhysicsContactDelegate {
    weak var adManagerDelegate: AdManagerDelegate?
    let gameData = GameData.shared
    
    var roof : SKSpriteNode!
    var statusBar : SKSpriteNode!
    var dangerZone : SKSpriteNode!
    var animationZone : VisualDisplayNode!
    var staminaBar : StaminaBarNode!
    var colorRamp : TouchableColorRamp!
    var hitFeedback : SKShapeNode!
    
    var selectedMode: GameModes? = nil
    
    var randomGameMode: RandomGameMode? = nil
    var questGameMode: QuestGameMode? = nil
    
    // var swipeRight = UISwipeGestureRecognizer()

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appBecameActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func willMove(from view: SKView) {
        print("moving out Game")
    }
    
    override func didMove(to view: SKView) {
        //gameData.parser.loadElements()
        print("moving in Game")
        setupPhysics()
        layoutScene()
        gameData.scene = self
        gameData.isPaused = false
        
//        swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(GameScene.swipedRight))
//        swipeRight.direction = .right
//        view.addGestureRecognizer(swipeRight)

        switch selectedMode! {
        case GameModes.Random:
            randomMode()
            break
        case GameModes.Quest:
            questMode()
            break
        case GameModes.Test:
            testMode()
            break
        case GameModes.Enemies:
            let EnemiesScene = EnemiesListScene(size: (self.scene?.size)!)
            let transition = SKTransition.fade(withDuration: 0.5)
            self.view?.presentScene(EnemiesScene, transition: transition)
            break
        }
    }
    
    override func update(_ currentTime: CFTimeInterval) {
        followMinXEnemy()
        
        if (self.scene != nil && self.questGameMode != nil && selectedMode == GameModes.Quest) {
            self.questGameMode!.update(currentTime: currentTime)
        }
    }

    
    
    //////////////////////////////////////////////
    ///////////GESTURES///////////////
    //////////////////////////////////////////////
    
    
//    @objc func swipedRight(_ sender: UIGestureRecognizer) {
//        print("Object has been swiped")
//        print(sender)
//        switch sender.state {
//         case .changed:
//             let touchInView = sender.location(in: sender.view)
//             let touch = convertPoint(fromView: touchInView)
//             print(self.colorRamp.contains(touch))
//             let nodeArray = nodes(at: touch)
//             for node in nodeArray {
//                 if node.name == "my shape node" {
//                     // You've got a reference to your shape node and can take some action here (add movement or whatever)
//                 }
//             }
//         default:
//             return
//         }
//     }
    

    //////////////////////////////////////////////
    ///////////////EVENTS/////////////////
    //////////////////////////////////////////////
    
    @objc func appMovedToBackground() {
        print("moved to background")
        if (!gameData.isPaused) {
            pauseGame()
        }
    }
    
    @objc func appBecameActive() {
        print("moved to Active")
    }
    
    //////////////////////////////////////////////
    ///////////GAME EVENTS/////////
    //////////////////////////////////////////////
    
    func playerGotHit(hits: Int) {
        gameData.player.loseLife(times: hits)
        
        diplayHitFeedback()
        // update life counter
        let livesNode = childNode(withName: NodeNames.LivesCounterParent)?.childNode(withName: NodeNames.LivesCounter) as! SKLabelNode
        livesNode.text = String(gameData.player.getLifes())
        
        print(gameData.player.getLifes())
        // is it the end?
        if gameData.player.isDead(){
            gameOver()
        }
    }
    
    @objc func pauseGame() {
        // let pauseScene = PauseScene(size: (self.scene?.size)!)
        // let transition = SKTransition.fade(withDuration: 0.5)
        // self.view?.presentScene(pauseScene, transition: transition)
        adManagerDelegate?.showMenuBanner()
        adManagerDelegate?.showInterstitial()
        gameData.isPaused = true
        scene?.isPaused = true
        
        switch selectedMode! {
        case GameModes.Random:
            self.randomGameMode!.pauseTimers()
            break
        case GameModes.Quest: break
            // TODO
        default: break
            // TODO
        }
        
        let side = frame.maxX / 1.5
        let pauseNode = PauseNode(rect: CGRect(x: frame.midX - side / 2, y: frame.midY - side / 2, width: side, height: side), cornerRadius: 20,scene: self)
        addChild(pauseNode.getPauseNode())
    }
    
    @objc func resumeGame() {
        // let pauseScene = PauseScene(size: (self.scene?.size)!)
        // let transition = SKTransition.fade(withDuration: 0.5)
        // self.view?.presentScene(pauseScene, transition: transition)
        adManagerDelegate?.hideMenuBanner()
        gameData.isPaused = false
        scene?.isPaused = false
        print("resume game")
        self.scene?.childNode(withName: "pause")?.removeFromParent()
        
        self.scene?.physicsWorld.speed = 1.0
        self.scene?.physicsWorld.gravity = CGVector(dx: 0, dy: -1.62)
        
        switch selectedMode! {
        case GameModes.Random:
            self.randomGameMode!.resumeTimers()
            break
        case GameModes.Quest: break
        // TODO
        default: break
            // TODO
        }
    }
    
    @objc func backToMenu() {
        switch selectedMode! {
        case GameModes.Random:
            self.randomGameMode!.cancelTimers()
            break
        case GameModes.Quest: break
        // TODO
        default: break
            // TODO
        }
        
        let transition = SKTransition.fade(withDuration: 0.5)
        let menuScene = MenuScene(size: self.size)
        self.view?.presentScene(menuScene, transition: transition)
    }
    
    func gameOver () {
        print("Game Over")
        adManagerDelegate?.showInterstitial()
        
        switch selectedMode! {
        case GameModes.Random:
            self.randomGameMode!.cancelTimers()
            self.removeAllChildren()
            break
        case GameModes.Quest: break
        // TODO
        default: break
            // TODO
        }
        
        let transition = SKTransition.fade(withDuration: 0.5)
        let gameOverScene = GameOverScene(size: self.size)
        gameOverScene.adManagerDelegate = adManagerDelegate
        gameOverScene.text = "Game Over"
        self.view?.presentScene(gameOverScene, transition: transition)
    }
    
    //////////////////////////////////////////////
    ///////////////////MODES//////////////////////
    //////////////////////////////////////////////

    func testMode() {

    }
    
    func questMode() {
        questGameMode = QuestGameMode(scene: self)
    }
    
    func randomMode() {
        randomGameMode = RandomGameMode(scene: self)
    }
    
    
    //////////////////////////////////////////////
    ///////////////////LAYOUT/////////////////////
    //////////////////////////////////////////////

    func layoutScene() {
        if(gameData.nightMode) {
            self.backgroundColor = AppColor.Day.Background
        } else {
            self.backgroundColor = AppColor.Night.Background
        }
        
        let levelPickerBg = "rainbow_test"
        //setBGAnimation()
        setHitFeedback()
        setColorPicker(pickerChoice : levelPickerBg)
        setStaminaBar()
        setAnimationNode()
        setRoof()
        setStatusBar()
    }
    
    private var bg = SKSpriteNode()
    private var bgFrames: [SKTexture] = []
    
    func setBGAnimation(){
        self.initializeFrames()
        buildBG()
        bg.run(SKAction.repeatForever(SKAction.animate(with: bgFrames,
                                                           timePerFrame: 0.3,
                                                           resize: false,
                                                           restore: true)))
    }
    
    private func buildBG() {
        let bg_forest = "bg_forest_1"
        bg = SKSpriteNode(texture: SKTexture(imageNamed: bg_forest))
        bg.size = CGSize(width: self.frame.width, height: self.frame.height)
        bg.color = .yellow
        bg.zPosition = -10
        bg.position = CGPoint.zero
        bg.anchorPoint = CGPoint.zero
        addChild(bg)
    }
   
    private func initializeFrames() {
        for i in 1...7 {
            bgFrames.append(SKTexture(imageNamed: "bg_forest_\(i)"))
        }
    }
    
    
    func setRoof() {
        roof = SKSpriteNode(color: UIColor.black, size: CGSize(width: frame.maxX, height: 1))
        roof.position = CGPoint(x: frame.midX, y: frame.maxY + 2)
        roof.physicsBody = SKPhysicsBody(rectangleOf: roof.size)
        roof.physicsBody?.isDynamic = false
        roof.physicsBody?.categoryBitMask = PhysicsCategories.roof
        roof.physicsBody?.contactTestBitMask = PhysicsCategories.waveFire
        roof.physicsBody?.collisionBitMask = PhysicsCategories.none
        roof.name = NodeNames.Roof
        addChild(roof)
    }
    
    func setStatusBar() {
        statusBar = SKSpriteNode(color: UIColor.blue, size: CGSize(width: frame.maxX, height: frame.size.height/22))
        statusBar.position = CGPoint(x: frame.midX, y: frame.maxY - statusBar.size.height / 2)

        // addChild(statusBar)
        // pause
        let pauseButtonTexture: SKTexture! = SKTexture(imageNamed: "Pause_icon")
        let pauseButton = FTButtonNode(normalTexture: pauseButtonTexture, selectedTexture: pauseButtonTexture, disabledTexture: pauseButtonTexture, size: CGSize(width: statusBar.size.height / 1.5, height: statusBar.size.height / 1.5))
        pauseButton.setButtonAction(target: self, triggerEvent: .TouchUpInside, action: #selector(pauseGame))
        pauseButton.position = CGPoint(x: (scene?.frame.minX)! + 40, y: statusBar.position.y)
        pauseButton.size = CGSize(width: statusBar.size.height / 1.5, height: statusBar.size.height / 1.5)
        pauseButton.zPosition = 1
        
        addChild(pauseButton)
        
        // lives count

        let liviesImg = SKSpriteNode(texture: SKTexture(imageNamed: "Heart"), color: UIColor.clear, size: CGSize(width: statusBar.size.height / 1.5, height: statusBar.size.height / 1.5))
        liviesImg.position = CGPoint(x: (scene?.frame.maxX)! - 40, y: statusBar.position.y)
        liviesImg.name = NodeNames.LivesCounterParent
        
        let liveCount = SKLabelNode(fontNamed: "Thonburi")
        
        liveCount.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        liveCount.verticalAlignmentMode = SKLabelVerticalAlignmentMode.center
        liveCount.text = String(gameData.player.getLifes())
        liveCount.fontSize = 13
        liveCount.fontColor = SKColor.white
        liveCount.name = NodeNames.LivesCounter
        
        liviesImg.addChild(liveCount)

        self.addChild(liviesImg)
    }
    
    func setAnimationNode() {
        // aka deadline
        animationZone = VisualDisplayNode(size: CGSize(width: frame.maxX, height: 40))
        animationZone.position = CGPoint(x: frame.midX, y: colorRamp.size.height + staminaBar.size.height + animationZone.size.height / 2)
        animationZone.physicsBody = SKPhysicsBody(rectangleOf: animationZone.size)
        animationZone.physicsBody?.categoryBitMask = PhysicsCategories.dealine
        animationZone.physicsBody?.isDynamic = false
        animationZone.name = NodeNames.Deadline
        animationZone.anchorPoint = CGPoint.zero
        addChild(animationZone)
    }
    
    func setColorPicker(pickerChoice : String) {
        let pickerBgTexture = SKTexture(imageNamed: pickerChoice)
        colorRamp = TouchableColorRamp(texture: pickerBgTexture, size: CGSize(width: frame.size.width, height: frame.size.height/7))
        colorRamp.anchorPoint = CGPoint.zero
        colorRamp.position = CGPoint.zero
        colorRamp.isUserInteractionEnabled = true
        colorRamp.name = NodeNames.ColorRamp
        addChild(colorRamp)
    }
    
    func setStaminaBar() {
        staminaBar = StaminaBarNode(texture: nil, color: .clear, size:  CGSize(width: frame.maxX, height: 10))
        
        staminaBar.position = CGPoint(x: frame.midX, y: colorRamp.size.height + staminaBar.size.height / 2)
        staminaBar.isUserInteractionEnabled = false
        staminaBar.name = NodeNames.StaminaBar
        addChild(staminaBar)
    }
    
    
    //////////////////////////////////////////////
    //////////////////SUPPORT/////////////////////
    //////////////////////////////////////////////
    
    func setupPhysics() {
        physicsWorld.contactDelegate = self
    }
    
    
    func followMinXEnemy() {
        var destination: CGFloat? = nil
        var minY: CGFloat = self.frame.height + 1
        var hasBalls = false
        self.children.forEach {
            let node = ($0)
            if node is LittleEnemyNode || node is ArtifactNode {
                hasBalls = true
                if( node.position.y < minY && destination != CGFloat(node.position.x)) {
                    minY = node.position.y
                    destination = CGFloat(node.position.x)
                }
            }
        }
        if (hasBalls && destination != nil && destination! != animationZone.getPlayerXPosition()) {
            animationZone.animateWalkToPosition(x: destination!)
        } else {
            animationZone.stopAnimation()
        }
    }
    
    func setHitFeedback () {
        hitFeedback = SKShapeNode(rect: CGRect(x: frame.midX, y: frame.midY, width: 50, height: 50))
        hitFeedback.strokeColor = .red
        hitFeedback.lineWidth = 20
        hitFeedback.alpha = 0
        addChild(hitFeedback)
    }
    
    func diplayHitFeedback () {
        // TODO different levels of vibration
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        let fadeIn = SKAction.fadeAlpha(to: 0.7, duration: 0.3)
        let fadeOut = SKAction.fadeAlpha(to: 0, duration: 0.3)
        hitFeedback.run(fadeIn)
        hitFeedback.run(fadeOut)
    }
}
