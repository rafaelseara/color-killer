//
//  TouchableColorRamp.swift
//  Color Killer
//
//  Created by Rafael Seara on 09/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import SpriteKit

struct Pixel {
    var value: UInt32
    var red: UInt8 {
        get { return UInt8(value & 0xFF) }
        set { value = UInt32(newValue) | (value & 0xFFFFFF00) }
    }
    var green: UInt8 {
        get { return UInt8((value >> 8) & 0xFF) }
        set { value = (UInt32(newValue) << 8) | (value & 0xFFFF00FF) }
    }
    var blue: UInt8 {
        get { return UInt8((value >> 16) & 0xFF) }
        set { value = (UInt32(newValue) << 16) | (value & 0xFF00FFFF) }
    }
    var alpha: UInt8 {
        get { return UInt8((value >> 24) & 0xFF) }
        set { value = (UInt32(newValue) << 24) | (value & 0x00FFFFFF) }
    }
}

struct RGBA {
    var pixels: UnsafeMutableBufferPointer<Pixel>
    var width: Int
    var height: Int
    
    init?(image: UIImage) {
        guard let cgImage = image.cgImage else { return nil } // 1
        
        width = Int(image.size.width)
        height = Int(image.size.height)
        
        let bitsPerComponent = 8 // 2
        
        let bytesPerPixel = 4
        let bytesPerRow = width * bytesPerPixel
        let imageData = UnsafeMutablePointer<Pixel>.allocate(capacity: width * height)
        let colorSpace = CGColorSpaceCreateDeviceRGB() // 3
        
        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Big.rawValue
        bitmapInfo |= CGImageAlphaInfo.premultipliedLast.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
        guard let imageContext = CGContext(data: imageData, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo) else { return nil }
        imageContext.draw(cgImage, in: CGRect(origin: CGPoint.zero, size: image.size))
        
        pixels = UnsafeMutableBufferPointer<Pixel>(start: imageData, count: width * height)
    }
}

class TouchableColorRamp : SKSpriteNode {
    
    var shotMovement: [CGPoint] = []
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        shotMovement = []
        let touch = touches.first! as UITouch
        shotMovement.append(touch.location(in: self))
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if GameData.shared.isPaused {
            // do nothing
            print("paused")
        } else {
            let touch = touches.first! as UITouch
            shotMovement.append(touch.location(in: self))
            
            let move = processShotMovement(shotMovement: shotMovement)
            
            switch move {
                case SHOT_MOVS.NORMAL_SHOT:
                    print("normal")
                    if(GameData.shared.currentShots > 0) {
                        if let touch = touches.first {
                            let currentPoint = touch.location(in: self)
                            // do something with your currentPoint
                            //print("fireWave")
                            if let pixelColor = getPixelColor(position: currentPoint){

                                fireShot(colorFire: pixelColor, height: 4.0)
                                GameData.shared.scene?.staminaBar.rechargePauseableTimer.restart()
                            }
                        }
                    }
                    break
                case SHOT_MOVS.SWIPE_RIGHT:
                    if(GameData.shared.currentShots > 0) {
                        fireShot(colorFire: .white, height: 7.0)
                    }
                    break
                case SHOT_MOVS.SWIPE_LEFT:
                    print("left")
                    break
                default:
                    print("MOVEMENT NOT RECOGNIZED")
                    break
            }
        }
    }

    enum SHOT_MOVS {
        static let NORMAL_SHOT : String = "normal_shot"
        static let SWIPE_RIGHT : String = "swipe_right"
        static let SWIPE_LEFT : String = "swipe_left"

    }
    
    func processShotMovement(shotMovement: [CGPoint]) -> String {
        if (shotMovement.count >= 2) {
            let startingTouch =  shotMovement.first?.x
            let endingTouch = shotMovement.last?.x
                   
            let distance = floor(endingTouch!) - floor(startingTouch!)

            if (distance == 0) {
                return SHOT_MOVS.NORMAL_SHOT
            } else if (abs(distance) > 0.7 * self.frame.width) {
                
                print(abs(distance))
                print(distance)
                print(0.7 * self.frame.width)
                
                
                if (distance > 0) {
                    return SHOT_MOVS.SWIPE_RIGHT
                } else {
                    return SHOT_MOVS.SWIPE_LEFT
                }
            }
        }
        return SHOT_MOVS.NORMAL_SHOT
    }
    
    func fireShot(colorFire: UIColor, height: CGFloat){
        run(SKAction.playSoundFileNamed("RAD_FX_NANO_7", waitForCompletion: false))
        if (GameData.shared.scene?.staminaBar.isStaminaModeActivated() ?? false) {
            fireWave(colorFire: colorFire, height: height)
        } else {
            GameData.shared.currentShots -= 1
            fireWave(colorFire: colorFire, height: height)
        }
        
    }
    
    func fireWave(colorFire: UIColor, height: CGFloat) {
        let wave = SKSpriteNode(color: colorFire, size: CGSize(width: frame.maxX, height: height))
        
        setupShotPhysics(wave: wave)
        self.scene?.addChild(wave)

        let fireWaveAction = SKAction.moveTo(y: (scene?.frame.maxY)!, duration: TimeInterval(WAVE_SPEED))
        wave.run(
            SKAction.sequence([
                fireWaveAction,
                SKAction.removeFromParent()
                ])
        )
    }
    
    func setupShotPhysics(wave: SKSpriteNode) {
        wave.position = CGPoint(x: frame.midX, y: frame.maxY)
        wave.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: frame.maxX, height: wave.frame.height))
        wave.physicsBody?.affectedByGravity = false
        wave.physicsBody?.friction = 0
        wave.physicsBody?.linearDamping = 0
        wave.physicsBody?.allowsRotation = false
        wave.physicsBody?.categoryBitMask = PhysicsCategories.waveFire
        wave.physicsBody?.contactTestBitMask =
            PhysicsCategories.normalEnemy
            | PhysicsCategories.multEnemy
            | PhysicsCategories.iceCube
            | PhysicsCategories.meteorEnemy
            | PhysicsCategories.bomb
            | PhysicsCategories.colorAll
            | PhysicsCategories.shield
            | PhysicsCategories.invert
            | PhysicsCategories.life
            | PhysicsCategories.timingEnemy
            | PhysicsCategories.popEnemy
            | PhysicsCategories.stamina
        wave.physicsBody?.collisionBitMask = PhysicsCategories.none
        wave.name = NodeNames.WaveFire
    }
    
    func getPixelColor(position: CGPoint) -> UIColor?{
        let resizedImag = resizedImage(newSize: self.size, image:  UIImage(cgImage: (texture?.cgImage())!))
        let rgba = RGBA.init(image: resizedImag)
        
        let index = Int(position.y) * (rgba?.width)! + Int(position.x)
        
        if self.frame.contains(position) {
            guard let pixel = rgba?.pixels[index] else {
                return nil
            }
            let color = UIColor(red: CGFloat(pixel.red)/255, green: CGFloat(pixel.green)/255, blue: CGFloat(pixel.blue)/255, alpha: CGFloat(pixel.alpha)/255)
            return color
        }
        return nil
    }
    
    func resizedImage(newSize: CGSize, image: UIImage) -> UIImage {
        // Guard newSize is different
        guard image.size != newSize else { return image }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
