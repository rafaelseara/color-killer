//
//  SettingsDisplay.swift
//  Color Killer
//
//  Created by Rafael Seara on 14/07/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import Foundation
import SpriteKit

class SettingsNode : SKSpriteNode {
    
    let gameData = GameData.shared
    var initialPosition: CGPoint? = nil
    var settingsButtons: [FTButtonNode] = []
    var isExpanded = false
    let buttonsName = ["Info_icon", "Star_icon", "Music_icon", "Vibration_icon", "Language_icon"]
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(size: CGSize) {
        super.init(texture: nil, color: .clear, size: size)
        self.size = size
        initialPosition = CGPoint(x: self.frame.minX + size.height, y: self.frame.minY + size.height)
        
        addButton(imageName: "Settings", action: #selector(expandCollapseSettings))
        addButton(imageName: "Info_icon", action: #selector(infoButtonAction))
        addButton(imageName: "Star_icon", action: #selector(nightModeButtonAction))
        addButton(imageName: "Music_icon", action: #selector(musicButtonAction))
        addButton(imageName: "Vibration_icon", action: #selector(vibrationButtonAction))
        addButton(imageName: "Language_icon", action: #selector(languageButtonAction))

    }
    
    private func addButton(imageName: String, action: Selector) {
        let texture = SKTexture(imageNamed: imageName)
        let button = FTButtonNode(normalTexture: texture, selectedTexture: texture, disabledTexture: texture, size: self.size)
        button.name = imageName
        button.setButtonAction(target: self, triggerEvent: .TouchUpInside, action: action)
        button.position = initialPosition!
        if (imageName == "Settings") {
            button.zPosition = CGFloat(buttonsName.count)
            self.addChild(button)
        } else {
            settingsButtons.append(button)
        }
    }
    
    @objc func infoButtonAction() {
        print("info")
        self.scene?.backgroundColor = .purple
    }
    
    @objc func vibrationButtonAction() {
        print("info")
        self.scene?.backgroundColor = .purple
    }
    
    @objc func languageButtonAction() {
        print("info")
        self.scene?.backgroundColor = .purple
    }
    
    
    @objc func musicButtonAction() {
        print("music")
        self.scene?.backgroundColor = .red
    }
    
    @objc func nightModeButtonAction() {
        gameData.nightMode = !gameData.nightMode

        if(gameData.nightMode) {
            self.scene?.backgroundColor = AppColor.Day.Background
            (self.scene?.childNode(withName: "title") as! SKLabelNode).fontColor = AppColor.Night.Background

            (self.scene?.childNode(withName: GameModes.Enemies.name) as! StrokeButtonNode).label.fontColor = AppColor.Night.Background
            (self.scene?.childNode(withName: GameModes.Quest.name) as! StrokeButtonNode).label.fontColor  = AppColor.Night.Background
            (self.scene?.childNode(withName: GameModes.Random.name) as! StrokeButtonNode).label.fontColor  = AppColor.Night.Background
            (self.scene?.childNode(withName: GameModes.Test.name) as! StrokeButtonNode).label.fontColor = AppColor.Night.Background
            
            (self.scene?.childNode(withName: GameModes.Enemies.name) as! StrokeButtonNode).shape.strokeColor = AppColor.Night.Background
            (self.scene?.childNode(withName: GameModes.Quest.name) as! StrokeButtonNode).shape.strokeColor  = AppColor.Night.Background
            (self.scene?.childNode(withName: GameModes.Random.name) as! StrokeButtonNode).shape.strokeColor  = AppColor.Night.Background
            (self.scene?.childNode(withName: GameModes.Test.name) as! StrokeButtonNode).shape.strokeColor = AppColor.Night.Background
            


        } else {
            self.scene?.backgroundColor = AppColor.Night.Background
            (self.scene?.childNode(withName: "title") as! SKLabelNode).fontColor = AppColor.Day.Background
            
            (self.scene?.childNode(withName: GameModes.Enemies.name) as! StrokeButtonNode).label.fontColor = AppColor.Day.Background
            (self.scene?.childNode(withName: GameModes.Quest.name) as! StrokeButtonNode).label.fontColor  = AppColor.Day.Background
            (self.scene?.childNode(withName: GameModes.Random.name) as! StrokeButtonNode).label.fontColor  = AppColor.Day.Background
            (self.scene?.childNode(withName: GameModes.Test.name) as! StrokeButtonNode).label.fontColor = AppColor.Day.Background
            
            (self.scene?.childNode(withName: GameModes.Enemies.name) as! StrokeButtonNode).shape.strokeColor = AppColor.Day.Background
            (self.scene?.childNode(withName: GameModes.Quest.name) as! StrokeButtonNode).shape.strokeColor  = AppColor.Day.Background
            (self.scene?.childNode(withName: GameModes.Random.name) as! StrokeButtonNode).shape.strokeColor  = AppColor.Day.Background
            (self.scene?.childNode(withName: GameModes.Test.name) as! StrokeButtonNode).shape.strokeColor = AppColor.Day.Background
            
        }
    }
    
    @objc func expandCollapseSettings() {
        
        if (isExpanded) {
            for (_, element) in buttonsName.enumerated() {
                let buttonNode = self.childNode(withName: element)
                
                let futureY: CGFloat = (initialPosition!.y)
                let animationTime = 0.2
                
                let moveIconsUpAction = SKAction.moveTo(y: futureY, duration: TimeInterval(animationTime))
                buttonNode!.run(
                    SKAction.sequence([
                        moveIconsUpAction,
                        SKAction.removeFromParent()
                        ])
                )
            }
            isExpanded = false
            
        } else {
            if (self.hasElements()) {
                return
            }
            
            for (index, element) in settingsButtons.enumerated() {
                
                element.size = size
                element.position = initialPosition!
                element.zPosition = CGFloat(buttonsName.count - (index + 1))
                if ((element.parent) != nil) {
                    return
                }
                self.addChild(element)
                
                let futureY: CGFloat = (initialPosition!.y) - CGFloat(index + 1) * self.size.height - 15 * CGFloat(index + 1)
                let animationTime = 0.3
                
                let moveIconsUpAction = SKAction.moveTo(y: futureY, duration: TimeInterval(animationTime))
                element.run(
                    SKAction.sequence([
                        moveIconsUpAction
                        ])
                )
            }
            isExpanded = true
        }
    }
    
    private func hasElements() -> Bool {
        for element in buttonsName {
            if((self.childNode(withName: element)) != nil) {
                return true
            }
        }
        return false
    }
}
