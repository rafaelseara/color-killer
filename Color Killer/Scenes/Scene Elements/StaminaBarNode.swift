//
//  StaminaBar.swift
//  Color Killer
//
//  Created by Rafael Seara on 30/06/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import Foundation
import SpriteKit

class StaminaBarNode : SKSpriteNode {
    
    var FRAGMENTS: Int = 10
    var rechargePauseableTimer: PauseableTimer!
    var fragments: [SKSpriteNode]!
    var staminaMode: Bool = false
    var staminaPauseableTimer: PauseableTimer?
    var STAMINATIME: Int = 20
    var staminaTimeLeft: Int?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        GameData.shared.currentShots = 10
        
        let rechargeTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(recharge), userInfo: nil, repeats: true)
        rechargePauseableTimer = PauseableTimer(timer: rechargeTimer)
        fragmentation()
        print(fragments.count)
    }

    @objc func recharge() {
        if(GameData.shared.currentShots < FRAGMENTS) {
            if(fragments[GameData.shared.currentShots].parent == nil) {
                self.addChild(fragments[GameData.shared.currentShots])
                GameData.shared.currentShots += 1
            }
        }
    }
    
    func fragmentation() {
        let staminaFragmentWidth = size.width / CGFloat(FRAGMENTS)
        fragments = []
        for fragNumber in 0...(FRAGMENTS - 1) {
            let staminaFragment = SKSpriteNode(color: .random, size: CGSize(width: staminaFragmentWidth, height: 10))
            staminaFragment.anchorPoint = CGPoint(x: self.size.height / 2, y: 0.5)
            staminaFragment.position = CGPoint(x: CGFloat(fragNumber) * staminaFragmentWidth, y: 0)
            fragments.append(staminaFragment)
            self.addChild(staminaFragment)
        }
    }
    
    func removeBarSegment() {
        print("Bar removed")
        self.children.last?.removeFromParent()
    }
    
    func activateStaminaMode() {
        print("########## stamina mode activated")
        staminaMode = true
        staminaTimeLeft = STAMINATIME
        
        self.color = .white
        self.removeAllChildren()
        rechargePauseableTimer.invalidate()
        
        let staminaTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(stopStamina), userInfo: nil, repeats: true)
        staminaPauseableTimer = PauseableTimer(timer: staminaTimer)
    }
    
    @objc func stopStamina() {
        staminaTimeLeft! -= 1
        
        print(staminaTimeLeft!)
        if (staminaTimeLeft! <= 0) {
            print("########## stamina mode DEACTIVATED")
            staminaPauseableTimer?.invalidate()
            staminaMode = false
            fragmentation()
            rechargePauseableTimer.restart()
        }
    }

    func isStaminaModeActivated() -> Bool {
        return staminaMode
    }
}
