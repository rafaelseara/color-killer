//
//  Countdown.swift
//  Color Killer
//
//  Created by Rafael Seara on 25/05/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import Foundation
import SpriteKit

class CountdownLabel: SKLabelNode {
    
    let gameData = GameData.shared
    var countdownTimer = Timer()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init() {
        super.init()
        self.fontName = COUNTER_FONT
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCountdown), userInfo: nil, repeats: true)
    }
    
    override func removeFromParent() {
        countdownTimer.invalidate()
    }
    
    @objc
    func updateCountdown() {
        let currentDate = Date()
        if (gameData.player.getLifeTimer() != nil && gameData.player.getLifeTimer()! > currentDate) {
            let futureDate = gameData.player.getLifeTimer()
            let components = Calendar.current.dateComponents([.minute, .second], from: currentDate, to: futureDate!)
            let formatMinutes = components.minute! < 10 ? "0" + String(components.minute!) : String(components.minute!)
            let formatSeconds = components.second! < 10 ? "0" + String(components.second!) : String(components.second!)
            self.text = formatMinutes + ":" + formatSeconds
        } else {
            countdownTimer.invalidate()
            gameData.player.revive()
            self.fontName = GAME_FONT
            self.text = "You won 3 lifes"
        }
    }
}



