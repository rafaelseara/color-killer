//
//  UnlockScene.swift
//  Color Killer
//
//  Created by Rafael Seara on 25/10/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import SpriteKit
import GoogleMobileAds

class UnlockScene: SKScene {
    
    let gameData = GameData.shared
    weak var adManagerDelegate: AdManagerDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        if(gameData.nightMode) {
            self.backgroundColor = AppColor.Day.Background
        } else {
            self.backgroundColor = AppColor.Night.Background
        }
        
        displayAchievementName()
        displayGameOverVideoAdd(size: size)
        displayContinueButton()
    }
    
    func displayAchievementName() {
        let achievementLabel = SKLabelNode(fontNamed: GAME_FONT_BOLD)
        achievementLabel.text = "NEW ENEMY"
        achievementLabel.fontSize = 50.0
        achievementLabel.name = "title"
        achievementLabel.fontColor = gameData.nightMode ? AppColor.Night.Background : AppColor.Day.Background
        achievementLabel.position = CGPoint(x: self.frame.midX, y: (self.frame.maxY - self.frame.midY / 2))
        addChild(achievementLabel)
    }
    
    func displayContinueButton() {
        let button = StrokeButtonNode(normalColor: gameData.nightMode ? AppColor.Night.Background : AppColor.Day.Background, selectedColor: .white, disabledColor: .gray, size: CGSize(width: self.frame.maxX / 3, height: 40), cornerRadius: 20)
        button.setButtonLabel(title: "Continue", font: GAME_FONT, fontSize: 20)
        button.setButtonAction(target: self, triggerEvent: .TouchUpInside, action: #selector(continueGame))
        button.position = CGPoint(x: self.size.width / 2, y: self.size.height / 6)
        button.name = "continue"
        self.addChild(button)
    }
    
    @objc func continueGame() {
        adManagerDelegate?.hideMenuBanner()
        let transition = SKTransition.fade(withDuration: 0.5)
        let menuScene = MenuScene(size: self.size)
        menuScene.adManagerDelegate = adManagerDelegate
        self.view?.presentScene(menuScene, transition: transition)
        
    }
        
    override func didMove(to view: SKView) {
        adManagerDelegate?.showMenuBanner()
    }
    
    override func willMove(from view: SKView) {
        print("Moving out game over")
    }
    
    func displayGameOverVideoAdd(size: CGSize) {
        let containerHeightFactor = CGFloat(0.8)
        let containerSize = CGSize(width: size.width * containerHeightFactor , height: size.width * containerHeightFactor)
        let advContainerNode = SKSpriteNode(color: UIColor.green, size: containerSize)
        advContainerNode.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        addChild(advContainerNode)
    }
}
