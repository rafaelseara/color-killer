//
//  StrokeButtonNode.swift
//  Color Killer
//
//  Created by Rafael Seara on 16/06/2019.
//  Copyright © 2019 Rafael Seara. All rights reserved.
//

import SpriteKit

class StrokeButtonNode: SKShapeNode {
    
    enum StrokeButtonActionType: Int {
        case TouchUpInside = 1,
        TouchDown, TouchUp
    }
    
    var isEnabled: Bool = true {
        didSet {
            if (disabledColor != nil) {
                shape.fillColor = (isEnabled ? defaultColor : disabledColor)!
            }
        }
    }
    var isSelected: Bool = false {
        didSet {
            shape.fillColor = (isSelected ? selectedColor : defaultColor)!
            self.label.fontColor = (isSelected ? .black : selectedColor)!

        }
    }
    
    var defaultColor: UIColor?
    var selectedColor: UIColor?
    var label: SKLabelNode
    
    required init(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    var shape: SKShapeNode!
    
    init(normalColor defaultColor: UIColor!, selectedColor: UIColor!, disabledColor: UIColor?, size: CGSize, cornerRadius: CGFloat) {
        
        self.defaultColor = defaultColor
        self.selectedColor = selectedColor
        self.disabledColor = disabledColor
        self.label = SKLabelNode(fontNamed: "Helvetica");
        super.init()
        
        shape = SKShapeNode(rectOf: size, cornerRadius: cornerRadius)
        shape.strokeColor = defaultColor
        isUserInteractionEnabled = true
        
        //Creating and adding a blank label, centered on the button
        self.label.verticalAlignmentMode = SKLabelVerticalAlignmentMode.center;
        self.label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center;
        self.label.zPosition = 1
        self.label.fontColor = defaultColor
        shape.addChild(self.label)
        addChild(shape)

        
    }
    
    /**
     * Taking a target object and adding an action that is triggered by a button event.
     */
    func setButtonAction(target: AnyObject, triggerEvent event:StrokeButtonActionType, action:Selector) {
        
        switch (event) {
        case .TouchUpInside:
            targetTouchUpInside = target
            actionTouchUpInside = action
        case .TouchDown:
            targetTouchDown = target
            actionTouchDown = action
        case .TouchUp:
            targetTouchUp = target
            actionTouchUp = action
        }
        
    }
    
    /*
     New function for setting text. Calling function multiple times does
     not create a ton of new labels, just updates existing label.
     You can set the title, font type and font size with this function
     */
    
    func setButtonLabel(title: NSString, font: String, fontSize: CGFloat) {
        self.label.text = title as String
        self.label.fontSize = fontSize
        self.label.fontName = font
    }
    
    var disabledColor: UIColor?
    var actionTouchUpInside: Selector?
    var actionTouchUp: Selector?
    var actionTouchDown: Selector?
    weak var targetTouchUpInside: AnyObject?
    weak var targetTouchUp: AnyObject?
    weak var targetTouchDown: AnyObject?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (!isEnabled) {
            return
        }
        isSelected = true
        if (targetTouchDown != nil && targetTouchDown!.responds(to: actionTouchDown)) {
            UIApplication.shared.sendAction(actionTouchDown!, to: targetTouchDown, from: self, for: nil)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if (!isEnabled) {
            return
        }
        
        let touch: AnyObject! = touches.first
        let touchLocation = touch.location(in: parent!)
        
        if (frame.contains(touchLocation)) {
            isSelected = true
        } else {
            isSelected = false
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (!isEnabled) {
            return
        }
        
        isSelected = false
        
        if (targetTouchUpInside != nil && targetTouchUpInside!.responds(to: actionTouchUpInside!)) {
            let touch: AnyObject! = touches.first
            //let touchLocation = touch.location(in: parent!)
            print(actionTouchUpInside)
                UIApplication.shared.sendAction(actionTouchUpInside!, to: targetTouchUpInside, from: self, for: nil)
            
        }
        
        if (targetTouchUp != nil && targetTouchUp!.responds(to: actionTouchUp!)) {
            UIApplication.shared.sendAction(actionTouchUp!, to: targetTouchUp, from: self, for: nil)
        }
    }
    
}
