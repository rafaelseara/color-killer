//
//  GameViewController.swift
//  Color Killer
//
//  Created by Rafael Seara on 05/10/2018.
//  Copyright © 2018 Rafael Seara. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GoogleMobileAds
import AVFoundation

protocol AdManagerDelegate: class {
    func showMenuBanner()
    func hideMenuBanner()
    func showInterstitial()
}

class GameViewController: UIViewController, AdManagerDelegate, GADRewardBasedVideoAdDelegate, GADInterstitialDelegate {
    
    let gameData = GameData.shared

    
    var bannerView: GADBannerView!
    var rewardBasedAd: GADRewardBasedVideoAd!
    var interstitial: GADInterstitial!
        
    override func viewDidLoad() {
        super.viewDidLoad()
    
        rewardBasedAd = createAndLoadReward()
        interstitial = createAndLoadInterstitial()

        // Present the scene
        if let view = self.view as! SKView? {

            let menuScene = MenuScene(size: view.bounds.size)
            menuScene.scaleMode = .aspectFill
            menuScene.adManagerDelegate = self
            view.presentScene(menuScene)
            view.ignoresSiblingOrder = false
            gameData.controller = self
            // TODO remove debug info
            // setupDebugInfo(view: view)
        }
    }
    
    func showMenuBanner() {
        createMenuBanner()
        sendRequest()
    }
    
    func hideMenuBanner() {
        bannerView.removeFromSuperview()
    }
    
    func showInterstitial() {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }
    
    private func setupDebugInfo(view: SKView) {
        view.showsFPS = true
        view.showsNodeCount = true
        view.showsPhysics = true
    }
    
    func createMenuBanner() {
        // In this case, we instantiate the banner with desired ad size.
        // custom add size
        //let adSize = GADAdSizeFromCGSize(CGSize(width: self.view.frame.width, height: 50))
        // kGADAdSizeBanner or kGADAdSizeSmartBannerPortrait
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        addBannerViewToView(bannerView)
    }
    
    func sendRequest() {
        // TODO REMOVE
        let request = GADRequest()
        request.testDevices = [ "de2dcee925dbcaa5aea2d41e8c136987" ] // Sample device ID
        bannerView.load(request)
    }
    
    private func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                 attribute: .bottom,
                                 relatedBy: .equal,
                                 toItem: view,
                                 attribute: .bottom,
                                 multiplier: 1,
                                 constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func createAndLoadReward() -> GADRewardBasedVideoAd {
        let rewardBasedAd = GADRewardBasedVideoAd.sharedInstance()
        rewardBasedAd.delegate = self
        rewardBasedAd.load(
            GADRequest(), withAdUnitID: "ca-app-pub-3940256099942544/1712485313")
        return rewardBasedAd
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didRewardUserWith reward: GADAdReward) {
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd:GADRewardBasedVideoAd) {
        print("Reward based video ad is received.")
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Opened reward based video ad.")
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad started playing.")
    }
    
    func rewardBasedVideoAdDidCompletePlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad has completed.")
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad is closed.")
        rewardBasedAd = createAndLoadReward()
    }
    
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load.")
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}
